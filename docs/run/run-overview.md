# Run overview

The runs are related to jobs. On the **run overview** you get a list of runs related to the selected job. You can find
the run overviews on two locations in the project. The first location is on the
[project page in the section Jobs](../project.md#project-page). Here the jobs for that project are listed and
via the :octicons-chevron-down-16: you can expand a section that will list the runs related to that job.

The second location is on the [job page in the section Runs](../job/index.md#job-page). On both locations you will
find a table like the screenshot below.

![Run overview](../media/run/askanna-run-overview.png){ loading=lazy }

In this table you see:

| Name    | Description                                                                                               |
| ------- | --------------------------------------------------------------------------------------------------------- |
| SUUID   | The first four charactes of the run SUUID. If you put your mouse on the text you can also see the full SUUID and a copy button to copy the full SUUID. |
| Name    | If a name was set for the run, we show the name. If no name is set, we show an empty cell.                |
| Status  | The status of the run                                                                                     |
| Timing  | Information when the run started and what the duration was/is (depending on the status)                   |
| By      | The member who triggered the run                                                                          |
| Input   | The number of lines the input payload file has. When the payload is empty, it is reported as 1 line.      |
| Metrics | The number of metrics tracked for the run                                                                 |

When you click on a run, the related run page will open. If you click on any object on the row, the run page will
open. But if you specifically click on the `input` or `metrics`, we will directly open the tab `INPUT` or `METRICS` on
the related run page.

On the [job page in the section Runs](../job/index.md#job-page) you can also search and filter the run list. On this
page, you find a search and filter bar. Searching works on the run `SUUID` and `name`. You can also filter
the run list on:

- Created by
- Status
- Trigger

When you filter with the above attributes, you can filter with `is` or `is not` and select an available value. You can
also combine multiple filters and combine filters with search.

![Project runs section with a demo of search and
filter](../media/project/project-runs-search-filter.gif){ loading=lazy }
