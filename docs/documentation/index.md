# Documentation

In the documentation you can read about the different components in AskAnna and how you can use them to run your
projects. In the documentation you can read about:

- [Workspace](../workspace.md)
- [Project](../project.md)
- [Job](../job/index.md)
- [Run](../run/index.md)
- [Environment](../environment/index.md)
- [Code](../code.md)
- [Variable](../variable/index.md)
- [Tracking Variables](../variable/tracking.md)
- [Tracking Metrics](../metrics.md)
- [User account and profile](../account.md)
- [Permissions and roles](../permissions.md)
