# User account and profile

The user account contains information about you and your activity on AskAnna. Your account includes several settings
and profiles.

## Edit your profile

To access your profile in the right upper corner you can:

- click on your avatar or user image
- click `Edit my profile`

![Edit my profile](media/account/edit-my-profile.png){ width=200px, loading=lazy }

## Change login information

On the [Edit my profile](#edit-your-profile) page, you can change the:

1. Email
2. Password

Change the email or password and click on `SAVE MY CHANGES`.

When you changed the login information, you will receive an informative email. When you changed the email, the
info is send to the old email address.

## Profiles

In the profile, you set your:

1. Name
2. Title
3. Profile image

!!! info
    If you don't set a name, the email address used to log in will be shown in activity logs as name of the member.

On the [Edit my profile](#edit-your-profile) you can change these settings, or upload a new image. On the
[Edit my profile](#edit-your-profile) you see the tab `ASKANNA PROFILE`. Also, you might see two tabs:

1. *{ WORKSPACE NAME }* WORKSPACE
2. ASKANNA PROFILE

These tabs refer to the two types of account-profiles we have in AskAnna:

1. AskAnna profile
2. Workspace profile

The AskAnna profile is used by default. You can choose to set a specific name, title and profile image for a
workspace. This information can be changed in the workspace profile.

This setup makes it possible that you can use the same user account for professional and fun workspaces. Feel free to
set a funny avatar for your personal workspace and that you don't show in projects you do for your boss.

It doesn't mean you always have to make a workspace profile. By default the AskAnna profile is used:

![Workspace and AskAnna profile](media/account/default-profile.png){ width=300px, loading=lazy }

If you unselect the checkbox `Use the AskAnna profile`, you can edit the workspace profile or switch back and use
the AskAnna profile:

![Workspace and AskAnna profile](media/account/edit-profile.png){ width=300px, loading=lazy }

### Edit a profile

You can edit the profile by changing one of the fields:

- Name
- Title
- Image

Complete the edit action by clicking on `SAVE MY CHANGES`.

## Delete my account

Coming soon. If you want to delete your account, you can now [contact us](contact.md) and we will delete your account
as soon as possible.

!!! warning "Keep a copy in the workspace profile"
    If you delete your account, we keep a copy of the workspace profile. With this workspace profile info, other
    people will be able to reproduce results. Also if your AskAnna account is removed.

    In case your workspace profile is using the AskAnna profile, then a copy of the AskAnna profile is saved as the workspace profile. If you don't want that this info to be copied to the workspace profile, you can set other
    values in the AskAnna profile.
