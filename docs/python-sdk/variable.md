# Python SDK - Project Variable

## askanna.variable.get

```python
import askanna

variable = askanna.variable.get(variable_suuid="{VARIABLE_SUUID}")
```

<h3>Parameters</h3>

| Name           | Type   | Required | Description                                              |
| -------------- | ------ | -------- | -------------------------------------------------------- |
| variable_suuid | String | Yes      | Variable SUUID you want to retrieve the information from |

<h3>Output</h3>

[Dataclass: Variable](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/project.py)

## askanna.variable.list

```python
import askanna

variable_list = askanna.variable.list()
```

<h3>Parameters</h3>

| Name             | Type    | Required | Description                                                          |
| ---------------- | ------- | -------- | -------------------------------------------------------------------- |
| project_suuid    | String  | No       | SUUID of the project for which you want to get a list of variables   |
| workspace_suuid  | String  | No       | SUUID of the workspace for which you want to get a list of variables |
| is_masked        | Bool    | No       | Filter for masked variables                                          |
| number_of_result | Integer | No       | The number of variables in the result. Defaults to 100.              |
| order_by         | String  | No       | Sets the ordering of the variable list. Defaults to "-created_at".   |
| search           | String  | No       | Search for a specific variable                                       |

<h3>Output</h3>

List with elements with
[Dataclass: Variable](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/project.py)

## askanna.variable.create

```python
import askanna

variable = askanna.variable.create(
    project_suuid="{PROJECT_SUUID}",
    name="Variable name",
    value="Variable value",
)
```

<h3>Parameters</h3>

| Name          | Type    | Required | Description                                                          |
| ------------- | ------- | -------- | -------------------------------------------------------------------- |
| project_suuid | String  | Yes      | Project SUUID in which project you want to create the variable       |
| name          | String  | Yes      | Name for the new variable                                            |
| value         | String  | Yes      | Value you want to save for the variable                              |
| is_masked     | Boolean | No       | Specify if the value should be handled as masked. Defaults to False. |

<h3>Output</h3>

[Dataclass: Variable](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/project.py)

## askanna.variable.change

```python
import askanna

variable = askanna.variable.change(variable_suuid="{VARIABLE_SUUID}", name="New variable name")
```

<h3>Parameters</h3>

| Name           | Type    | Required | Description                                                          |
| -------------- | ------- | -------- | -------------------------------------------------------------------- |
| variable_suuid | String  | Yes      | The SUUID of the variable you want to change                         |
| name           | String  | No       | The new variable name                                                |
| value          | String  | No       | The new variable value                                               |
| is_masked      | Boolean | No       | Specify if the value should be handled as masked. Defaults to False. |

!!! info "Change `is_masked`"
    You can set a unmasked value to masked, but you **cannot** change a masked value into unmasked

<h3>Output</h3>

[Dataclass: Variable](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/project.py)

## askanna.variable.delete

```python
import askanna

askanna.variable.delete(variable_suuid="{VARIABLE_SUUID}")
```

<h3>Parameters</h3>

| Name           | Type   | Required | Description                                  |
| -------------- | ------ | -------- | -------------------------------------------- |
| variable_suuid | String | Yes      | The SUUID of the variable you want to delete |

<h3>Output</h3>

Bool: True if the variable is succesfully deleted
