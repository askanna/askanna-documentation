# Release notes

At AskAnna we release new features, changes and fixes once they are ready to deploy. On the Beta of AskAnna we always
run the latest approved code, also if it does not have an official version number yet. From time to time we create a
new version. In the release notes you will find AskAnna versions and everything we release to Beta that is not part of
an official version. These non-version releases will be listed here with the date of deployment to Beta.

In these release notes we give updates about the AskAnna platform. Besides the platform, we also have the AskAnna
Python project that contains the [AskAnna CLI](../cli.md) and [Python SDK](../python-sdk/index.md). Release notes for
this project are available in
[the AskAnna Python changelog](https://gitlab.com/askanna/askanna-python/-/blob/master/CHANGELOG.md) published on
GitLab.com.

## Coming soon

We're in the process of revamping AskAnna's file storage system. This overhaul will introduce the flexibility to
configure storage locations. Initially, we'll roll out optional object storage to MinIO bucket.

This extension is a step towards expanding the AskAnna CLI, including a new feature to execute and monitor local runs
We welcome your suggestions on tracking code, data, and experiments. Feel free to share your ideas with us via [email](mailto:hello@askanna.io)!

## Released

### December 2023

<h4>New</h4>

- On the workspace and project page, you can now view the workspace/project's SUUID, creation date-time, creator
  and description by clicking on the arrow in the upper right corner.
- Introduced a "scroll to top" feature to make it faster to go to the top of the page.

<h4>Changed</h4>

- Enhanced the User Experience of the web interface, including improved readability.
- Reduced the number of API calls made by using the web interface.
- Added avatar information for the 'created_by' field to the workspace and project information in the API response.

<h4>Removed</h4>

- Removed the Intercom widget on the web interface and documenation.

### July 2023

<h4>Fixed</h4>

- Fixed a bug where created_at caused an error in API list responses using a previous link

### June 2023

<h4>Fixed</h4>

- Fixed not loading history package on empty created by

### April 2023

<h4>New</h4>

- Added the ability to search and filter all runs on the [project](../project.md#runs) and
  [job](../run/run-overview.md) page.
- Implemented new run filters for `status`, `trigger`, `created_by`, and `package_suuid`. These filters can be used in
  the web interface, Python SDK, and API.
- Created a new query option to support excluding values for filters (e.g. `status__exclude`) for more fine-grained
  filtering.
- Added a new filter option for `status` to the `askanna run list` CLI command.

<h4>Fixed</h4>

- Fixed a bug with run image name that had duplication of tags in the web interface. With the fix we show the correct
  image name.

### March 2023

<h4>New</h4>

- The project page has a new `RUNS` section where you can find all the project runs
- New integrations for data sources on the Google Cloud Platform:
    - [Google BigQuery](../integrations/data-sources/google-bigquery.md)
    - [Google Cloud Storage](../integrations/data-sources/google-cloud-storage.md)

<h4>Changed</h4>

- Add '_at'-suffix to date-time fields
- Simplify and rename base models in Django app core
- Upgrade packages in AskAnna Backend:
    - Django to 4.0
    - `dj-rest-auth` and implement new setup of settings
    - Replace `pytz` with `zoneinfo`

<h4>Fixed</h4>

- Fix search on runs in Django admin
- Fix design on sign up form so buttons always look good

### February 2023

<h4>New</h4>

- First open source release of the AskAnna Frontend and new CI/CD jobs to publish new releases to
  [GitLab](https://gitlab.com/askanna/askanna-frontend) and [GitHub](https://github.com/askanna-io/askanna-frontend)
- Extend the list of file extensions for which we cannot show a preview (yet): h5, pkl, parquet, app, exe, com, dll,
  dmg, iso, jar, zip, ko, lib, so, dat, bin and cab

<h4>Changed</h4>

- For rendering Markdown content set breaks property to false to render Markdown conform the official specs
- Rename users app to account
- Align printing of object info via CLI

<h4>Fixed</h4>

- Fix creating a project using the CLI with empty description
- Refactor run info to fix showing avatar from created by on run page
- Edit job description

### January 2023

<h4>New</h4>

- Added search and new filter options to the API endpoints
- PDF files can be viewed
- Search for a run on the run table
- Tables can now be ordered by clicking on the column
- New API endpoint to check if an email is already related to a member of the workspace
- Extend CLI commands:
    - info option to retrieve info from an object
    - `askanna run list` to get a table with recent runs
    - support for search workspace, project, job, variable and run
- Python SDK supports new query options, a.o. to search for workspace, project, job, variable and run.

<h4>Changed</h4>

- Backend / API:
    - Switched to cursor pagination instead of using offset and limit.
    - Order by is now case insensitive
    - Structurally implemented filters and removed endpoints that were used as a filter
    - API docs are now up-to-date ([check them out](https://beta-api.askanna.eu/v1/docs/swagger/))
    - Removed most of the nested endpoints. Filters can be used to get subsets of objects.
    - Aligned responses, a.o. how we show related object information.
    - Refactor Role Based Permission and apply it also to the views which had customer permission logic to check role
      based access.
- Frontend:
    - Support new cursor pagination
    - Refactor to load less data by default, so pages should load faster
    - Search for workspaces & projects is now using the API endpoint search option
    - Refactored validation for the invitation email
    - Refactor related to the new API endpoint setup
    - Replace Moment with DayJS
    - Blocked double-clicking on the sign-in button to prevent double sign-in requests

<h4>Fixed</h4>

- Viewing Jupyter Notebooks
- When a schedule is missed, a notification will be sent
- Cached images that are locally not available anymore are now pulled again instead of triggering an error
- In the frontend the documentation links are working again
- Added validation rule that terms of use must be accepted before creating a new account
- Code and artifact file lists now show the first directories and then files.
