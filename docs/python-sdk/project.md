# Python SDK - Project

## askanna.project.get

```python
import askanna

project = askanna.project.get(project_suuid="{PROJECT_SUUID}")
```

<h3>Parameters</h3>

| Name          | Type   | Required | Description                                             |
| ------------- | ------ | -------- | ------------------------------------------------------- |
| project_suuid | String | Yes      | Project SUUID you want to retrieve the information from |

<h3>Output</h3>

[Dataclass: Project](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/project.py)

## askanna.project.list

```python
import askanna

project_list = askanna.project.list()
```

<h3>Parameters</h3>

| Name             | Type    | Required | Description                                                           |
| ---------------- | ------- | -------- | --------------------------------------------------------------------- |
| is_member        | Bool    | No       | Filter on projects where the authenticated user is a member.          |
| visibility       | String  | No       | Filter on projects with a specific visibility ("PRIVATE" or "PUBLIC") |
| workspace_suuid  | String  | No       | Workspace SUUID for which you want to get a list of projects          |
| number_of_result | Integer | No       | The number of projects in the result. Defaults to 100.                |
| order_by         | String  | No       | Sets the ordering of the project list. Defaults to "-created_at".     |
| search           | String  | No       | Search for a specific project                                         |

<h3>Output</h3>

List with elements with
[Dataclass: Project](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/project.py)

## askanna.project.create

```python
import askanna

project = askanna.project.create(workspace_suuid="{WORKSPACE_SUUID}", name="Project name")
```

<h3>Parameters</h3>

| Name            | Type   | Required | Description                                                                   |
| --------------- | ------ | -------- | ----------------------------------------------------------------------------- |
| workspace_suuid | String | Yes      | SUUID of the workspace to create the project in                               |
| name            | String | Yes      | Name for the new project                                                      |
| description     | String | No       | Description for the new project. Defaults to an empty description.            |
| visibility      | String | No       | Visibility of the new project. Allowed values are "PRIVATE" and "PUBLIC". Defaults to "PRIVATE". |

<h3>Output</h3>

[Dataclass: Project](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/project.py)

## askanna.project.change

```python
import askanna

project = askanna.project.change(project_suuid="{PROJECT_SUUID}", name="New project name")
```

<h3>Parameters</h3>

| Name          | Type   | Required | Description                                                                     |
| ------------- | ------ | -------- | ------------------------------------------------------------------------------- |
| project_suuid | String | Yes      | SUUID of the project you want to change                                         |
| name          | String | No       | New name for the project. Defaults to None (do not change).                     |
| description   | String | No       | New description for the project. Defaults to None (do not change).              |
| visibility    | String | No       | Visibility of the project. Allowed values are "PRIVATE", "PUBLIC" or None. Defaults to None (do not change). |

!!! note "To change project info, at least we need a new `name`, `description` or `visibility`"

<h3>Output</h3>

[Dataclass: Project](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/project.py)

## askanna.project.delete

```python
import askanna

askanna.project.delete(project_suuid="{PROJECT_SUUID}")
```

<h3>Parameters</h3>

| Name          | Type   | Required | Description                             |
| ------------- | ------ | -------- | --------------------------------------- |
| project_suuid | String | Yes      | SUUID of the project you want to delete |

<h3>Output</h3>

Bool: True if the project is succesfully deleted
