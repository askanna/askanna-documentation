# Python SDK

The lateste version of the AskAnna SDK is publised on PyPi:

[![PyPi AskAnna](https://img.shields.io/pypi/v/askanna.svg)](https://pypi.org/project/askanna/){target="_blank"}

## Install

When working on a local project, we advice you to use a virtual environment and install the AskAnna package in this
environment: [https://docs.python.org/3/tutorial/venv.html](https://docs.python.org/3/tutorial/venv.html){target="_blank"}

You can install the latest version of AskAnna via:

```shell
pip install askanna
```

## Upgrade

If you want to upgrade to the latest version of AskAnna you can run:

```shell
pip install askanna --upgrade
```

## Login

To be able to interact with the AskAnna platform, you first need to login. After installing the AskAnna package, you
can login via the command line:

```shell
askanna login
```

Or in your Python code:

```python
from askanna.gateways.auth import AuthGateway

auth = AuthGateway()
auth.login(email="{EMAIL}", password="{PASSWORD}")
```

## Modules

In AskAnna the following modules are available:

* [Run](run.md)
* [Workspace](workspace.md)
* [Project](project.md)
* [Job](job.md)
* [Variable (project)](variable.md)
* [Track run data](tracking.md)
* [Result](result.md)
