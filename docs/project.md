# Project

An AskAnna project is a central location where you can collaborate and track the work done. You can work with your
team, or use it individually. You can house files, [code](code.md), [jobs](job/index.md), [runs](run/index.md),
[variables](variable/index.md) and all (meta) data related to running your projects.

## Project page

When you open a project, you see three sections on the page:

1. [Code](#code)
2. [Runs](#runs)
3. [Jobs](#jobs)
4. [Variables](#variables)

![Project page](media/project/askanna-project-page.png){ loading=lazy }

### Code

In the **Code** section, you find code available for the project. When you open the code section, the latest pushed
code will be presented. You can open the files. In the table you find the next information:

| Title    | Description                                            |
| -------- | ------------------------------------------------------ |
| Name     | Name of the file/directory                             |
| Size     | Size of the file/directory                             |
| Modified | When the file/directory was modified for the last time |

You can find more information in the [code documentation](code.md).

### Runs

In the **Runs** section, you will find a list of runs available for that project. In this table you see:

| Name    | Description                                                                                               |
| ------- | --------------------------------------------------------------------------------------------------------- |
| SUUID   | The first four charactes of the run SUUID. If you put your mouse on the text you can also see the full SUUID and a copy button to copy the full SUUID. |
| Job     | The job that was run. If you put your mouse you can also see the job SUUID and a copy button to copy the job name. |
| Name    | If a name was set for the run, we show the name. If no name is set, we show an empty cell.                |
| Status  | The status of the run                                                                                     |
| Timing  | Information when the run started and what the duration was/is (depending on the status)                   |
| By      | The member who triggered the run                                                                          |
| Input   | The number of lines the input payload file has. When the payload is empty, it is reported as 1 line.      |
| Metrics | The number of metrics tracked for the run                                                                 |

Above the table you find a search and filter bar. Searching works on the run `SUUID` and `name`. You can also filter
the run on:

- Created by
- Job name
- Status
- Trigger

When you filter with the above attributes, you can filter with `is` or `is not` and select an available value. You can
also combine multiple filters and combine filters with search.

![Project runs section with a demo of search and filter](media/project/project-runs-search-filter.gif){ loading=lazy }

### Jobs

In the **Jobs** section, you find a list of jobs available for that project. You can find more information in the
[job documentation](job/index.md).

### Variables

In the **Variables** section, you find the variables that you can use in your runs. Every variable is available as an
environment variable in the run environment for every job you run in AskAnna. You can find more information in the
[variables documentation](variable/index.md).

## Create a project

You can create an AskAnna project via the web interface, CLI, Python SDK and API. For every method, the project
information in the table below is relevant when creating a project:

| Attribute   | Required | Description                                                              |
| ----------- | -------- | ------------------------------------------------------------------------ |
| Name        | Yes      | The name of the project                                                  |
| Description | No       | A description of the project                                             |
| Workspace   | Yes      | The workspace where you want to create the project in.                   |
| Visibility  | No       | By default the visibility is `PRIVATE`, it could also be set to `PUBLIC` |
| Template    | No       | A project template (only available in the CLI)                           |

When creating a new project, the description, visiblity and template are optional. Using a project template is
currently only available via the AskAnna CLI. In the section [project template](#project-template) you can read more
about the templates.

When you use the web interface of AskAnna, or in case you are member of a single workspace, AskAnna will know in which
workspace you want to create a project. If you use the AskAnna CLI, and you are a member of multiple workspaces, we
will ask you in which workspace you want to create the project.

### Web interface

You can create a project via the Web Interface by going to the workspace home page. Here you can create a new project
by clicking on the button ++"\+ CREATE PROJECT"++. You can fill in a project name which will directly create the
project or you can choose for ++"MORE OPTIONS"++ where you can also fill in the project description and visiblity.

![Create project](media/project/askanna-create-projects.png){ loading=lazy }

### Command line (CLI)

If you create a project via the CLI you can choose to use the commands:

```shell
askanna create
```

```shell
askanna init
```

#### askanna create

With `askanna create`, you can create a new project in a new local directory. It will also create a project with the
provided name in the AskAnna workspace.

The AskAnna CLI will ask for a:

- project name (required)
- description (optional: you can leave it empty)
- workspace in case you are a member of multiple workspaces

Next, it will create a new project in AskAnna. Also, it will create a new local directory with the slugified version of
the project name. In this new directory you find the `askanna.yml` that already has the `push-target` set to the newly
created project.

You can also run `askanna create` with options:

```shell
askanna create "Project name" -d "Project description (optional)" -w "{WORKSPACE_SUUID}"
```

The full set of options are:

| Option                 | Description                                                      |
| ---------------------- | ---------------------------------------------------------------- |
| "name"                 | Name of the project                                              |
| -d, --description      | Description of the project                                       |
| -w, --workspace        | Workspace SUUID where you want to create the project             |
| -t, --template         | Location of a Cookiecutter [project template](#project-template) |
| -p, --push / --no-push | Push an initial version of the code [default: no-push]           |

With the option `--template`, you can use a project template. You can use our project templates, but also create your
own templates. This can be useful in case you want to always start with the same base or if you want to reuse code for
new projects quickly. See [project templates](#project-template) for more information.

With the option `--push`, you can specify that after creating the project you also want to push an initial version of
the code. When you start a project from a project template that already contains a job definition, this can be
powerfull.

#### askanna init

With `askanna init`, you can create a new project in your current directory. This can be usefull in case you already
have files / code for your project.

The AskAnna CLI will ask for:

- project name (required)
- description (optional: you can leave it empty)
- workspace in case you are a member of multiple workspaces

The CLI will create a new project in AskAnna. In your current directory the CLI create a new file `askanna.yml` with
the `push-target` for the new created project. If your project directory already contains a file `askanna.yml` the CLI
will inform you that it could not create this file, and that you need to add the push-target manually to be able to
push code using the CLI to AskAnna.

You can also run `askanna init` with options:

```shell
askanna init "Project name" -d "Project description (optional)" -w "{WORKSPACE_SUUID}"
```

The full set of options are:

| Option                 | Description                                                      |
| ---------------------- | ---------------------------------------------------------------- |
| "name"                 | Name of the project                                              |
| -d, --description      | Description of the project [optional; default is empty]          |
| -w, --workspace        | Workspace SUUID where you want to create the project [optional]  |
| -t, --template         | Location of a Cookiecutter [project template](#project-template) |

### Python SDK

With the [Python SDK](python-sdk/project.md#askannaprojectcreate) you can create a project via:

```python
from askanna import project

project = project.create(workspace_suuid="{WORKSPACE_SUUID}", name="Project name")
```

### API

An authenticated user can POST a request for creating a new project via:

`POST https://beta-api.askanna.eu/v1/project/`

The JSON body should contain at least the project name and workspace SUUID:

```json
{
    "name": "Project name",
    "description": "Project description (optional)",
    "workspace": "{WORKSPACE_SUUID}",
    "visibility": "PRIVATE",  # (or "PUBLIC")
}
```

If creating a project via the post was successful, AskAnna will respond with an HTTP-code 201 and will give back the
project information.  

## Project template

One of the ways we want to help you to kickstart your new project, is by making it possible to start a new project from
a template. This feature is currently only available with the AskAnna CLI:

```shell
askanna create "Project name" -t "{LOCATION_OF_TEMPLATE}"
```

On [our open source section on GitLab](https://gitlab.askanna.io/open/project-templates){target="_blank"}, you can find
all project templates that are currently available. A short list:

| Name                  | Location of the template                                                                    |
| --------------------- | ------------------------------------------------------------------------------------------- |
| Blanco template       | [https://gitlab.com/askanna/project-templates/blanco-template.git][t1]{target=_blank}       |
| Data Science template | [https://gitlab.com/askanna/project-templates/data-science-template.git][t2]{target=_blank} |
| AskAnna Demo Project  | [https://gitlab.com/askanna/demo/askanna-demo-project.git][t3]{target=_blank}               |
| Demo multiple models  | [https://gitlab.com/askanna/demo/demo-multiple-models.git][t4]{target=_blank}               |
| Demo train, select and serve a model | [https://gitlab.com/askanna/demo/demo-train-select-serve-model.git][t5]{target=_blank} |

[t1]: https://gitlab.com/askanna/project-templates/blanco-template.git
[t2]: https://gitlab.com/askanna/project-templates/data-science-template.git
[t3]: https://gitlab.com/askanna/demo/askanna-demo-project.git
[t4]: https://gitlab.com/askanna/demo/demo-multiple-models.git
[t5]: https://gitlab.com/askanna/demo/demo-train-select-serve-model.git

When you use the AskAnna CLI to create a new project, by default we will use
[the Blanco template](https://gitlab.com/askanna/project-templates/blanco-template/-/tree/master).

For the templates we make use of [Cookiecutter](https://github.com/audreyr/cookiecutter){target="_blank"}. If you want
to make your own project template, we advise you to
[copy our Blanco template](https://gitlab.com/askanna/project-templates/blanco-template/-/tree/master){target="_blank"}
and modify it for your own project template.

The location of our templates, is the same as the HTTPS Git URL. You can also work with private repositories as long as
you have access to the repo on the system where you run `#!shell askanna create "PROJECT NAME" -t "REPO URL"`. Check
[the Cookiecutter documentation](https://cookiecutter.readthedocs.io/en/latest/usage.html){target="_blank"}
for more information about the usage it supports.

## Project visibility

The project visibility can be set to [Private](#private) or [Public](#public). In
[Edit project info and settings](#edit-project-info-and-settings) you can find how to change this setting.

### Private

By default project visibility is set to `Private`. When the project visibility is set to private, this means that only
[workspace members](workspace.md#workspace-members) can open the project.

### Public

If the [workspace visibility](workspace.md#workspace-visibility) is set to [public](workspace.md#public), then you
will also have the option to set project visibility to `Public`. This means that all project information including
code, jobs, variables and runs becomes visible for everyone. Also for people without an account in AskAnna.

!!! info "Exposing credentials"
    We don't expose masked variables. Also in public projects, you can store a password in a variable. Of course, if
    you run a script that print your password to the log, you can still expose sensitive information.

## Edit project info and settings

Via the web interface, CLI, Python SDK and API, you can edit the information of a project.

### Web interface

On the project page you can click on :material-dots-vertical:. In this menu select `EDIT THIS PROJECT` and you can
update the project info and settings.

![Edit workspace](media/project/edit-project-info.gif){ loading=lazy }

!!! tip "Markdown support"
    The text editor support Markdown instructions. These instructions will automatically transferred into formatted
    text.

### Command line (CLI)

With the [AskAnna CLI](cli.md) you can change the project info with your command line. In your terminal run:

```shell
askanna project change
```

Optionally, you can also directly provide the info you want to change:

```shell
askanna project change --id "{PROJECT_SUUID}" --description "New project description"
```

### Python SDK

With the [Python SDK](python-sdk/index.md) you can edit a project via:

```python
from askanna import project

project = project.change(project_suuid="{PROJECT_SUUID}", name="New project name")
```

### API

An authenticated user can PATCH a project via:

`PATCH https://beta-api.askanna.eu/v1/project/{PROJECT_SUUID}/`

The JSON body should contain the fields you want to update:

{
    "name": "New project name",
    "description": "New project description",
    "visibility": "PRIVATE",  # (or "PUBLIC")
}

If changing a project was successful, AskAnna will respond with an HTTP-code 200 and will give back the updated
project information.

## Remove a project

You can remove a project via the web interface, CLI, Python SDK and API. When you remove a project, this is what
happens:

- you remove the project from the workspace
- all code, files and data related to the project will be removed
- if other projects or systems use jobs, files or other parts of the Demo project, they are not able to use it anymore

!!! warning "Recovery period of 30 days"
    When you remove a project, you will not have access to the project anymore. After 30 days, the system will
    entirely delete the project from the backend. Within 30 days, you can [contact us](contact.md) if you want to undo
    the removal.

    If the project and related files should be removed as soon as possible, you can [contact us](contact.md).

### Web interface

In the web interface you can remove a project from two locations:

1. Workspace home by clicking on :material-dots-vertical: a project card
1. Project page by clicking on :material-dots-vertical: on the right side

### Command line (CLI)

With the [AskAnna CLI](cli.md) you can remove a project on the command line. In your terminal run:

```shell
askanna project remove --id "{PROJECT_SUUID}"
```

### Python SDK

With the [Python SDK](python-sdk/project.md#askannaprojectdelete) you can delete a project via:

```python
from askanna import project

project.delete(project_suuid="{PROJECT_SUUID}")
```

### API

An authenticated user can DELETE a project via:

`DELETE https://beta-api.askanna.eu/v1/project/{PROJECT_SUUID}/`

If deleting a project was successful, AskAnna will respond with an HTTP-code 204.
