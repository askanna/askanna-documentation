---
hide:
  - navigation
---

# Get started with AskAnna

<div class="embed-content">
    <iframe class="responsive-iframe" src="https://player.vimeo.com/video/563696142"
            frameborder="0" allow="fullscreen; picture-in-picture" allowfullscreen></iframe>
</div>

AskAnna is a platform that helps you to kickstart your next Data Science project. In AskAnna, you can track, trace and
compare your runs. You can start projects using templates. And we have a set of tools that will allow you to run your
jobs in the cloud, including MLOps solutions.

Why did we build AskAnna? As Data Scientists, we understand how time-consuming getting your work in production is.
Creating your model, bringing it to work, and briefing the software engineers. But no more! We are committed to making
the best Data Science tools to improve your efficiency, collaboration, and speed. Let’s bring back the focus on the
things you love doing!

In AskAnna, you can organize your [projects](project.md) in [workspaces](workspace.md). We will explain how you can use
AskAnna to manage [jobs](job/index.md) and check the [runs](run/index.md).

You can work with AskAnna using our web platform. Also, we provide an [AskAnna CLI](cli.md),
[Python SDK](python-sdk/index.md) and API endpoints. In the examples, we share the different options how you can
interact with the AskAnna platform.

[Let's build an example project](examples/hello-askanna.md){: .md-button .md-button--primary }

!!! info
    We work hard to improve AskAnna. We cannot do this without your input. You can mention bugs, ideas, improvements...
    and also what you like. [Please send us an email](mailto:support@askanna.io).

## Do you want an account?

[Sign up and try AskAnna for free](https://beta.askanna.eu/signup/){: .md-button .md-button--primary }

For more information about the conditions check our [pricing](https://askanna.io/pricing/) and read the
[terms of use](https://askanna.io/terms/).
