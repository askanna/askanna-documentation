# Compare runs

The compare runs page is an initial version that helps you to compare runs directly in AskAnna. You can find the
compare runs link on the job page in the section [Runs](run-overview.md). On this page, you find a
:material-format-columns:-button which you can use to open the compare runs page.

![Compare runs page](../media/run/askanna-compare-runs.png){ loading=lazy }

On the compare runs page, you see the runs to be compared in the columns. The rows contain the run data and the data
is grouped (see table below). Each group can be expanded or collapsed. The groups are similar to the sections you get
on the [run page](run-page.md). The groups on the compare runs page are:

| Group          | Description                                                                                        |
| ------------------------------------------- | --------------------------------------------------------------------- |
| Overview                                    | Information about the start time, duration, code version, who triggered the run and the environment used |
| Input^*:material-information-variant:*^     | Meta information of the input used                                    |
| Result^*:material-information-variant:*^    | Meta information about the result                                     |
| Metrics^*:material-information-variant:*^   | Metrics can be compared by value. Each row shows the metric that matches based on the metric name and the label names + label values. By default we show the first 25 metrics. You can load more variables via the `SHOW MORE METRICS`-button. |
| Variables^*:material-information-variant:*^ | Variables can be compared by value. Each row shows the variable that matches based on the variable name and the label names + label values. By default we show the first 25 variables. You can load more variables vi the `SHOW MORE VARIABLES`-button. |
| Artifact^*:material-information-variant:*^  | Meta information about the artifact                                   |

*:material-information-variant:: If none of the selected runs has input, result, metrics, variables or artifact, then
you will see an informational message like `Artifact: these runs don't have an artifact` and the option to expand or
collapse the section is disabled.*

In the initial version, the compare runs page shows the last `15 runs`. If you are working on a larger screen, you can
get the full-width page by clicking the :octicons-screen-full-24:-button.
