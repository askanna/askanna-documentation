# Python SDK - Run

## Run a job

### askanna.run.start

```python
import askanna

askanna.run.start(job_name="{NAME_OF_THE_JOB}")

# or use the unique job SUUID:
askanna.run.start(job_suuid="{JOB_SUUID}")
```

<h4>Parameters</h4>

| Name          | Type       | Required | Description                                                   |
| ------------- | ---------- | -------- | ------------------------------------------------------------- |
| job_suuid     | String     | No *     | SUUID of the job you want to run                              |
| name          | String     | No       | Give the run a name                                           |
| descriptoin   | String     | No       | Add a description to the run info                             |
| data          | Dictionary | No       | JSON data containg an optional payload for the run            |
| job_name      | String     | No *     | Name of the job you want to run                               |
| project_suuid | String     | No       | Project SUUID of the project for which you want to run a job  |

!!! info "* To be able to start a run, we need either the `job_name` or the `job_suuid`."

<h4>Output</h4>

[Dataclass: RunStatus](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/run.py)

### askanna.run.status

```python
import askanna

status = askanna.run.status(run_suuid="{RUN_SUUID}")
print(status)
```

<h4>Parameters</h4>

| Name      | Type   | Required | Description                                      |
| --------- | ------ | -------- | ------------------------------------------------ |
| run_suuid | String | No       | SUUID of the run you want to get the status from |

!!! info "`run_suuid` not provided"
    If the `run_suuid` is not provided, the SDK will check if you started a run in the active session and will get the
    status of that run. If there is no "active" run, the SDK will raise an `ValueError`  with `No run SUUID set`.

<h4>Output</h4>

[Dataclass: RunStatus](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/run.py)

## Get run data

### askanna.run.get

```python
import askanna

run = askanna.run.get(run_suuid="{RUN_SUUID}")
```

<h4>Parameters</h4>

| Name              | Type    | Required | Description                                                           |
| ----------------- | ------- | -------- | --------------------------------------------------------------------- |
| run_suuid         | String  | No       | Run SUUID of the run you want to get the run info from                |
| include_metrics   | Boolean | No       | Should the output include the metrics, or not. Default value: False   |
| include_variables | Boolean | No       | Should the output include the variables, or not. Default value: False |

!!! info "`run_suuid` not provided"
    If the `run_suuid` is not provided, the SDK will check if you started a run in the active session and will get the
    info of that run. If there is no "active" run, the SDK will raise an `ValueError`  with `No run SUUID set`.

<h4>Output</h4>

[Dataclass: Run](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/run.py) & optionally
metrics and variables values

### askanna.run.list

```python
import askanna

run_list = askanna.run.list(run_suuid_list=["{RUN_SUUID_1}", "{RUN_SUUID_2}"])
```

<h4>Parameters</h4>

<h5>General parameters</h5>

| Name                      | Type    | Required | Description                                                        |
| ------------------------- | ------- | -------- | ------------------------------------------------------------------ |
| include_metrics           | Boolean | No       | Include the run metrics in the Run dataclass. Defaults to False.   |
| include_variables         | Boolean | No       | Include the run variables in the Run dataclass. Defaults to False. |
| number_of_result          | Integer | No       | The number of runs in the result. Defaults to 100.                 |
| order_by                  | String  | No       | Sets the ordering of the run list. Defaults to "-created_at"       |
| search                    | String  | No       | Search for a specific run                                          |

<h5>Filter parameters</h5>

| Name                      | Type    | Required | Description                                                        |
| ------------------------- | ------- | -------- | ------------------------------------------------------------------ |
| created_by_suuid          | String  | No       | SUUID of the member who started the run                            |
| created_by_suuid__exclude | String  | No       | Exclude runs started by this member SUUID                          |
| job_name                  | String  | No       | Name of the job from which you want to get the runs                |
| job_suuid                 | String  | No       | SUUID of the job from which you want to get the runs               |
| job_suuid__exclude        | String  | No       | Exclude runs with this job SUUID                                   |
| package_suuid             | String  | No       | SUUID of the package from which you want to get runs               |
| package_suuid__exclude    | String  | No       | Exclude runs with this package SUUID                               |
| project_suuid             | String  | No       | SUUID of the project from which you want to get the runs           |
| project_suuid__exclude    | String  | No       | Exclude runs with this project SUUID                               |
| run_suuid_list            | List    | No       | List of SUUIDs of the runs you want to get the runinfo from        |
| run_suuid__exclude        | String  | No       | Exclude runs with this run SUUID                                   |
| status                    | String  | No       | Filter the runs by status                                          |
| status__exclude           | String  | No       | Filter the runs by excluding status                                |
| trigger                   | String  | No       | Filter the runs by trigger                                         |
| trigger__exclude          | String  | No       | Filter the runs by excluding trigger                               |
| workspace_suuid           | String  | No       | SUUID of the workspace from which you want to get the runs         |
| workspace_suuid__exclude  | String  | No       | Exclude runs with this workspace SUUID                             |

!!! info "Return all results"
    When the paramters `run_suuid_list`, `project_suuid`, `job_suuid` and `job_name` are left blank, the function will
    return all runs with a default of 100 results.

<h4>Output</h4>

A list with
[Dataclass: Run](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses.py) objects &
optionally metrics and variables values

### askanna.run.get_metric

```python
import askanna

run_metrics = askanna.run.get_metric(run_suuid="{RUN_SUUID}")
```

<h4>Parameters</h4>

| Name      | Type   | Required | Description                                                 |
| --------- | ------ | -------- | ----------------------------------------------------------- |
| run_suuid | String | No       | Run SUUID of the run from which you want to get the metrics |

!!! info "`run_suuid` not provided"
    If the `run_suuid` is not provided, the SDK will check if you started a run in the active session and will get the
    metrics of that run. If there is no "active" run, the SDK will raise an `ValueError`  with `No run SUUID set`.

<h4>Output</h4>

[Dataclass: MetricList](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/run.py)

On this list it's possible to get a single object, or to filter multiple objects:

```python
run_metrics.get(name="{METRIC_NAME}")  # get a single object

run_metrics.filter(name="{METRIC_NAME}")  # filter multiple objects
```

### askanna.run.get_variable

```python
import askanna

run_variables = askanna.run.get_variable(run_suuid="{RUN_SUUID}")
```

<h4>Parameters</h4>

| Name      | Type   | Required | Description                                                   |
| --------- | ------ | -------- | ------------------------------------------------------------- |
| run_suuid | String | No       | Run SUUID of the run from which you want to get the variables |

!!! info "`run_suuid` not provided"
    If the `run_suuid` is not provided, the SDK will check if you started a run in the active session and will get the
    variables of that run. If there is no "active" run, the SDK will raise an `ValueError`  with `No run SUUID set`.

<h4>Output</h4>

[Dataclass: VariableList](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/run.py)

On this list it's possible to get a single object, or to filter multiple objects:

```python
run_variables.get(name="{VARIABLE_NAME}")  # get a single object

run_variables.filter(name="{VARIABLE_NAME}")  # filter multiple objects
```

### askanna.run.result

```python
import askanna

result = askanna.run.result(run_suuid="{RUN_SUUID}")
```

<h4>Parameters</h4>

| Name        | Type           | Required | Description                                                                                             |
| ----------- | -------------- | -------- | ------------------------------------------------------------------------------------------------------- |
| run_suuid   | String         | No       | SUUID of the run you want to get the result from                                                        |
| output_path | Path or String | No       | Path to save the result to. Default is `None`, which means the function will return the result content. |

!!! info "`run_suuid` not provided"
    If the `run_suuid` is not provided, the SDK will check if you started a run in the active session and will get the
    result of that run. If there is no "active" run, the SDK will raise an `ValueError`  with `No run SUUID set`.

<h4>Output</h4>

If `output_path` is `None` the output is the result content, which is of type `bytes`.

If `output_path` is set, then the output is the result saved at the `output_path` and the function returns `None`.

### askanna.run.result_content_type

```python
import askanna

result = askanna.run.result_content_type(run_suuid="{RUN_SUUID}")
```

<h4>Parameters</h4>

| Name      | Type    | Required | Description                                                   |
| --------- | ------- | -------- | ------------------------------------------------------------- |
| run_suuid | String  | No       | SUUID of the run you want to get the result content type from |

!!! info "`run_suuid` not provided"
    If the `run_suuid` is not provided, the SDK will check if you started a run in the active session and will get the
    result of that run. If there is no "active" run, the SDK will raise an `ValueError`  with `No run SUUID set`.

<h4>Output</h4>

`string` with the result content type

### askanna.run.artifact

```python
import askanna

artifact = askanna.run.artifact(run_suuid="{RUN_SUUID}")
```

<h4>Parameters</h4>

| Name        | Type           | Required | Description                                                                                                 |
| ----------- | -------------- | -------- | ----------------------------------------------------------------------------------------------------------- |
| run_suuid   | String         | No       | SUUID of the run you want to get the artifact from                                                          |
| output_path | Path or String | No       | Path to save the artifact to. Default is `None`, which means the function will return the artifact content. |

!!! info "`run_suuid` not provided"
    If the `run_suuid` is not provided, the SDK will check if you started a run in the active session and will get the
    artifact of that run. If there is no "active" run, the SDK will raise an `ValueError`  with `No run SUUID set`.

<h4>Output</h4>

If `output_path` is `None` the output is the artifact content, which is of type `bytes`.

If `output_path` is set, then the output is the artifact saved at the `output_path` and the function returns `None`.

### askanna.run.artifact_info

```python
import askanna

artifact_info = askanna.run.artifact_info(run_suuid="{RUN_SUUID}")
```

<h4>Parameters</h4>

| Name      | Type   | Required | Description                                             |
| --------- | ------ | -------- | ------------------------------------------------------- |
| run_suuid | String | No       | SUUID of the run you want to get the artifact info from |

!!! info "`run_suuid` not provided"
    If the `run_suuid` is not provided, the SDK will check if you started a run in the active session and will get the
    artifact info of that run. If there is no "active" run, the SDK will raise an `ValueError`  with
    `No run SUUID set`.

<h4>Output</h4>

[Dataclass: ArtifactInfo](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/run.py)

### askanna.run.payload

```python
import askanna

artifact = askanna.run.payload(run_suuid="{RUN_SUUID}")
```

<h4>Parameters</h4>

| Name        | Type           | Required | Description                                                                                                 |
| ----------- | -------------- | -------- | ----------------------------------------------------------------------------------------------------------- |
| run_suuid   | String         | No       | SUUID of the run you want to get the payload from                                                           |
| output_path | Path or String | No       | Path to save the artifact to. Default is `None`, which means the function will return the artifact content. |

!!! info "`run_suuid` not provided"
    If the `run_suuid` is not provided, the SDK will check if you started a run in the active session and will get the
    payload of that run. If there is no "active" run, the SDK will raise an `ValueError`  with `No run SUUID set`.

<h4>Output</h4>

If `output_path` is `None` the output is the payload content, which is of type `bytes`.

If `output_path` is set, then the output is the payload saved at the `output_path` and the function returns `None`.

### askanna.run.payload_info

```python
import askanna

payload_info = askanna.run.payload_info(run_suuid="{RUN_SUUID}")
```

<h4>Parameters</h4>

| Name      | Type   | Required | Description                                            |
| --------- | ------ | -------- | ------------------------------------------------------ |
| run_suuid | String | No       | SUUID of the run you want to get the payload info from |

!!! info "`run_suuid` not provided"
    If the `run_suuid` is not provided, the SDK will check if you started a run in the active session and will get the
    payload info of that run. If there is no "active" run, the SDK will raise an `ValueError`  with
    `No run SUUID set`.

<h4>Output</h4>

[Dataclass: Payload](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/run.py)

### askanna.run.log

```python
import askanna

log = askanna.run.log(run_suuid="{RUN_SUUID}")
```

<h4>Parameters</h4>

| Name      | Type   | Required | Description                                   |
| --------- | ------ | -------- | --------------------------------------------- |
| run_suuid | String | No       | SUUID of the run you want to get the log from |

!!! info "`run_suuid` not provided"
    If the `run_suuid` is not provided, the SDK will check if you started a run in the active session and will get the
    log of that run. If there is no "active" run, the SDK will raise an `ValueError`  with `No run SUUID set`.

<h4>Output</h4>

A list with log rows

## Change run info

### askanna.run.change

```python
import askanna

run = askanna.run.change(run_suuid="{RUN_SUUID}", name="New run name")
```

<h4>Parameters</h4>

| Name        | Type   | Required | Description                                                   |
| ----------- | ------ | -------- | ------------------------------------------------------------- |
| run_suuid   | String | No       | Run SUUID of the run you want to change the run info for      |
| name        | String | No       | New name for the run. Defaults to None (do not change).       |
| description | String | No       | New description of the run. Defaults to None (do not change). |

!!! note "To change run info, at least we need a new `name` or `description`"

!!! info "`run_suuid` not provided"
    If the `run_suuid` is not provided, the SDK will check if you started a run in the active session and will change
    the info of that run. If there is no "active" run, the SDK will raise an `ValueError`  with `No run SUUID set`.

<h4>Output</h4>

[Dataclass: Run](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/run.py)

## Delete a run

### askanna.run.delete

```python
import askanna

run = askanna.run.delete(run_suuid="{RUN_SUUID}")
```

<h4>Parameters</h4>

| Name      | Type   | Required | Description                             |
| --------- | ------ | -------- | --------------------------------------- |
| run_suuid | String | No       | Run SUUID of the run you want to delete |

!!! info "`run_suuid` not provided"
    If the `run_suuid` is not provided, the SDK will check if you started a run in the active session and will delete
    that run. If there is no "active" run, the SDK will raise an `ValueError`  with `No run SUUID set`.

<h4>Output</h4>

Bool: True if the run is succesfully deleted
