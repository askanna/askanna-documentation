# Examples

Sometimes an example can help you to get practical insight into how you can do something. With the examples in the
AskAnna documentation, we want to show how you can use our platform.

The current examples are:

- [Run "Hello AskAnna"](hello-askanna.md)
- [Train, select and serve a model](train-select-serve-model.md)
- [Run & compare multiple models](multiple-models.md)
- [Train a model with TensorFlow](train-with-tensorflow.md)

In the documentation you can find background information about the examples. If you want to see the running projects,
you can find them in the [AskAnna demo workspace](https://beta.askanna.eu/6swz-ujcr-jQQw-SAdZ){ target=_blank }.

On our company site we also publish generic ["How to-dos"](https://askanna.io/how-to/){ target=_blank }. For example,
you can find how to:

- [Install Python](https://askanna.io/how-to/2021/install-python-and-run-code/)
- [Use virtual environments for Python packages](https://askanna.io/how-to/2021/virtual-environments/)
- [Use requirements.txt to manage Python packages](https://askanna.io/how-to/2021/installing-python-packages/)

If you have an example that you want to add to the AskAnna documentation, feel free to open a merge request on our
[GitLab AskAnna Documentation project](https://gitlab.com/askanna/askanna-documentation){ target=_blank }.
