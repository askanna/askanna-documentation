# Run a Job

In AskAnna you can trigger jobs using the web interface, CLI, Python SDK or API. On the [job page](index.md#job-page)
you can find information about how you can run a job in AskAnna via the platform (web interface), curl and Python.
When new options become available, we will extend the list.

If you run a job, optionally you can add some additional information:

- Name
- Description
- Input JSON data

You can give your run a **name** and **description**. The name of the run is also shown in the overview of runs and
can help you to trace back a run. In the description you can add more information about the run. You can also edit
this information after the run to update it with what happened during the run.

The input type we now support is **JSON data**. When you trigger a run, you can add JSON data as input. You can read
more about how to use this input (payload) data on the page ["Create a Job"](create-job.md#job-definition).

## Web interface

The fastest and easiest way of triggering a job is via the web interface. On the tab `PLATFORM`, you can optionally
provide additional information for the run:

- a name to make it easier to trace back a run
- description
- JSON data

When you run the job, you will see the status of the run and a button to open the run.

![Run a job via the web interface](../media/job/askanna-run-job-platform.png){ loading=lazy }

## AskAnna CLI

First, install [the AskAnna CLI](../cli.md#install). Now you can start a job via the command line. When you run
`askanna run` you need to answer some questions that will help you to start a run:

```shell
askanna run
```

If you are running the command from a project directory with an `askanna.yml` and the `push-target` set to a valid
project, the CLI will ask you which job you want to run. If this information is not available, the CLI will check
whether you are a member of multiple workspaces or projects. If that's the case, a wizard guide you to select a job to
run.

To make life easier, you can also specify the name of the job:

```shell
askanna run "JOB NAME"
```

Similar to the [web interface](#web-interface), you can add a name and description for the run:

```shell
askanna run --name "NAME TO TRACE RUN" --description "A DESCRIPTION" "JOB NAME"
```

Also, you can add JSON data as a string:

```shell
askanna run --data '{"example": "payload"}' "JOB NAME"
```

Or JSON data from a file:

```shell
askanna run --data-file "input/payload.json" "JOB NAME"
```

If you want to push your code, and then run the job you can combine these tasks in one command:

```shell
askanna run --push "JOB NAME"
```

For more options, you can read the help:

```shell
askanna run --help
```

## Python

### AskAnna SDK

First, install [the Python AskAnna SDK](../python-sdk/index.md#install). Then you can run a job in your Python script
using the job SUUID:

```python
from askanna import run

run_info = run.start(job_suuid="{JOB_SUUID}")
```

When you run the code from a project directory with an `askanna.yml` and the `push-target` set to a valid project, you
can also use the name of the job:

```python
from askanna import run

run_info = run.start(job_name="JOB NAME")
```

If you want to give the run a name and add a description, you can start a run with:

```python
from askanna import run

run_info = run.start(
    job_name="JOB NAME",
    name="Run name",
    description="Run description"
)
```

It's also possible to include a dictionary with JSON data as input for the run:

```python
from askanna import run

json_data = {"example": "payload"}
run_info = run.start(job_name="JOB NAME", data=json_data)
```

### Request

In the tab `PYTHON` you can find an example of how you can use the AskAnna API to run a job in Python. When you trigger
a job via Python, you can use the response for follow-up actions. For example to monitor when the run finished so you
can retrieve the result.

![Run a job via Python](../media/job/askana-run-job-python.png){ loading=lazy }

## API

You can trigger jobs using our API. You can find all the information you need on the [job page](index.md#job-page) in
the section `Running the job`. For the API you will find a `Shell` example. In the example you see:

- The URL including the job SUUID
- Your authorization token
- An example data payload

When you copy this code, you only need to change the data (second row) and you are ready to run it via your terminal.
In case you are going to work often with APIs, we advise using a software platform like
[Postman](https://postman.com){ target="_blank" }.

The JSON data is optional. You can also run jobs without JSON data. If you add a JSON data body to the API request,
it will become available as the payload (input) for the run. This way you can add data, variables, or both.
