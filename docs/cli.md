# AskAnna CLI

The lateste version of the AskAnna CLI is publised on PyPi:

[![PyPi AskAnna](https://img.shields.io/pypi/v/askanna.svg)](https://pypi.org/project/askanna/){ target="_blank" }

## Install

When working on a local project, we advice you to use a virtual environment and install the AskAnna package in this
environment:
[https://docs.python.org/3/tutorial/venv.html](https://docs.python.org/3/tutorial/venv.html){ target="_blank" }

You can install the latest version of AskAnna via:

```shell
pip install askanna
```

## Log in

When you [installed](#install) the CLI, it's time to login. You can log in using your account's email address and
password.

```shell
askanna login
```

!!! tip
    When you login we create a local configuration file `.askanna.yml`. You can find this file in your local home
    directory. From this file you can copy the AskAnna token. You can use this token to authenticate the API, or for
    example to setup a integration with [GitLab](integrations/gitlab-github.md) or
    [GitHub](integrations/gitlab-github.md).

## Log out

You can log out. With this action, we remove the token from the `.askanna.yml` configuration file.

```shell
askanna logout
```

## Upgrade

If you want to upgrade to the latest version of AskAnna you can run:

```shell
pip install askanna --upgrade
```

## Commands

In the CLI the following commands are available for `askanna`:

| Command                                    | Description                                                          |
| ------------------------------------------ | -------------------------------------------------------------------- |
| [artifact](run/index.md#get-artifact)     | Download run artifacts                                               |
| [create](project.md#askanna-create)       | Create a project in a new directory                                  |
| [init](project.md#askanna-init)           | Create a project in the current directory                            |
| [job](job/index.md#cli)                   | Manage job in AskAnna                                                |
| [login](#log-in)                           | Login and save your AskAnna API key in a local file (~/.askanna.yml) |
| [logout](#log-out)                         | Remove saved AskAnna API key                                         |
| [project](project.md#cli)                 | Manage project in AskAnna                                            |
| [push](code.md#push-code)                  | Push code to AskAnna                                                 |
| [result](run/result.md#askanna-cli)       | Download run result                                                  |
| [run](job/run-job.md#askanna-cli)         | Run a job on AskAnna                                                 |
| [variable](variable/index.md#askanna-cli) | Manage variables in AskAnna                                          |
| [workspace](workspace.md#cli)              | Manage workspace in AskAnna                                          |

If there is more documentation about how to use a command, the command has a link to that section of the documentation.
Also, you can run `askanna --help` or `askanna COMMAND --help` to read instructions about the commands and options that
are available.
