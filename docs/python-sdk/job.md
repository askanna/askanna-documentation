# Python SDK - Job

## askanna.job.get

```python
import askanna

job = askanna.job.get(job_suuid="{JOB_SUUID}")
```

<h3>Parameters</h3>

| Name            | Type   | Required | Description                                    |
| --------------- | ------ | -------- | ---------------------------------------------- |
| job_suuid       | String | Yes      | SUUID of the job you want to get the info from |

<h3>Output</h3>

[Dataclass: Job](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/job.py)

## askanna.job.get_job_by_name

```python
import askanna

job = askanna.job.get_job_by_name(job_name="{NAME_OF_JOB}")
```

<h3>Parameters</h3>

| Name          | Type   | Required | Description                                                               |
| ------------- | ------ | -------- | ------------------------------------------------------------------------- |
| job_name      | String | Yes      | Name of the job you want to get the job info from                         |
| project_suuid | String | No       | Project SUUID of the project for which you want to get the job info from. |

<h3>Output</h3>

[Dataclass: Job](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/job.py)

!!! info "Multiple jobs with the same name"
    In case multiple jobs with the same name exist, you will see an error message. In that case you can add the
    project_suuid for which you want to get the job information.

## askanna.job.list

```python
import askanna

job_list = askanna.job.list()
```

<h3>Parameters</h3>

| Name             | Type    | Required | Description                                                               |
| ---------------- | ------- | -------- | ------------------------------------------------------------------------- |
| project_suuid    | String  | No       | Project SUUID of the project for which you want to get a list of jobs     |
| workspace_suuid  | String  | No       | Workspace SUUID of the workspace for which you want to get a list of jobs |
| number_of_result | Integer | No       | The number of jobs in the result. Defaults to 100.                        |
| order_by         | String  | No       | Sets the ordering of the job list. Defaults to "-created_at".             |
| search           | String  | No       | Search for a specific job                                                 |

<h3>Output</h3>

List with elements with
[Dataclass: Job](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/job.py)

## askanna.job.change

```python
import askanna

job = askanna.job.change(job_suuid="{JOB_SUUID}", name="New job name")
```

<h3>Parameters</h3>

| Name        | Type   | Required | Description                         |
| ----------- | ------ | -------- | ----------------------------------- |
| job_suuid   | String | Yes      | SUUID of the job you want to change |
| name        | String | No       | New name for the job                |
| description | String | No       | New description for the job         |

!!! note "To change job info, at least we need a new `name` or `description`"

??? warning "Changed the job name? Then update the `askanna.yml` as well."
    Changing the name of a job can be useful in case you want to rename your job. If you are going to rename the job,
    don't forget to also apply the change in the [askanna.yml](../code.md#askannayml).

    If you don't do this, you will not be able to run the job anymore because there is no matching job definition in
    the [askanna.yml](../code.md#askannayml) config. And if you don't change it in the
    [askanna.yml](../code.md#askannayml) file, the next time you push a new version of your code to AskAnna we will
    create a new job using the old name.

<h3>Output</h3>

[Dataclass: Job](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/job.py)

## askanna.job.delete

```python
import askanna

askanna.job.delete(job_suuid="{JOB_SUUID}")
```

<h3>Parameters</h3>

| Name      | Type   | Required | Description                         |
| --------- | ------ | -------- | ----------------------------------- |
| job_suuid | String | Yes      | SUUID of the job you want to delete |

<h3>Output</h3>

Bool: True if the job is succesfully deleted
