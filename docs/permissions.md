# Permissons and roles

Depending on the role a user has in a workspace, they will get different features. A user can have a membership in
multiple workspaces. Every membership comes with a role and the features assigned to this role. Per workspace the user
can get assigned a different role.

In the following tables we described per membership which action the user can perform.

## AskAnna membership

Every user has an AskAnna membership. Only people specifally assigned can have the `admin` role. By default every
user account gets the role `member` for the AskAnna membership. If you are not signed in, you will get the `guest`
role assigned.

The following table list the permissions per AskAnna membership role:

| Action                   | Guest | Member           | Admin (1)        |
| ------------------------ | ----- | ---------------- | ---------------- |
| Edit AskAnna profile (2) |       | :material-check: | :material-check: |
| Delete user account (2)  |       | :material-check: | :material-check: |
| Create workspaces        |       |                  | :material-check: |

Notes:

1. AskAnna admins don't have the power to access every workspace and project. In the platform we also have users with
   the permission `superuser`. These `superusers` can in theory access everything. Of course these users know that
   they should not do this.
1. You can only edit and delete your own account

## Workspace membership

The following table list the permissions per workspace membership role:

| Action                              | Guest                | Viewer           | Member           | Admin            |
| ----------------------------------- | -------------------- | ---------------- | ---------------- | ---------------- |
| View workspace info                 | :material-check: (1) | :material-check: | :material-check: | :material-check: |
| [Edit workspace info][w1]           |                      |                  |                  | :material-check: |
| [Remove workspace][w2]              |                      |                  |                  | :material-check: |
| Workspace project list              |                      | :material-check: | :material-check: | :material-check: |
| [Create project][w3]                |                      |                  | :material-check: | :material-check: |
| [View workspace people][w4]         |                      | :material-check: | :material-check: | :material-check: |
| [Invite people to the workspace][w5] |                     |                  | :material-check: | :material-check: |
| Resend invitiation                  |                      |                  | :material-check: | :material-check: |
| Change workspace member role        |                      |                  |                  | :material-check: |
| Remove member from a workspace      |                      |                  |                  | :material-check: |
| View member profile                 |                      | :material-check: | :material-check: | :material-check: |
| [Edit member profile][w6] (2)       |                      | :material-check: | :material-check: | :material-check: |
| Remove "my" workspace membership    |                      | :material-check: | :material-check: | :material-check: |

Notes:

1. A guest can only view workspace info if the workspace is set to allow public sharing
1. You can only edit your own account

[w1]: workspace.md#edit-workspace-info-and-settings
[w2]: workspace.md#remove-a-workspace
[w3]: project.md#create-a-project
[w4]: workspace.md#workspace-members
[w5]: workspace.md#invite-people
[w6]: account.md#edit-your-profile

## Project membership

The project membership is directly linked to the workspace membership. If you are an `admin` in the workspace, you are
also have the role `admin` in the project. Same goes for `member`. In the future it will be possible to assign
a higher role to the project membership, so that a workspace member can be a project admin.

The following table list the permissions per project membership role:

| Action                                  | Guest (1)        | Viewer           | Member           | Admin            |
| --------------------------------------- | ---------------- | ---------------- | ---------------- | ---------------- |
| View project info                       | :material-check: | :material-check: | :material-check: | :material-check: |
| [Edit project info][p1]                 |                  |                  |                  | :material-check: |
| [Remove project][p2]                    |                  |                  |                  | :material-check: |
| [Code: view][p3]                        | :material-check: | :material-check: | :material-check: | :material-check: |
| [Code: push or upload a new version][p4] |                 |                  | :material-check: | :material-check: |
| [Job: view job info][p5]                | :material-check: | :material-check: | :material-check: | :material-check: |
| [Job: create new job][p6]               |                  |                  | :material-check: | :material-check: |
| [Job: edit job info][p7]                |                  |                  | :material-check: | :material-check: |
| [Job: remove a job][p8]                 |                  |                  | :material-check: | :material-check: |
| [Variable: view variables][p9]          | :material-check: | :material-check: | :material-check: | :material-check: |
| [Variable: create a new variable][p10]  |                  |                  | :material-check: | :material-check: |
| [Variable: edit variables][p11]         |                  |                  | :material-check: | :material-check: |
| [Variable: remove a variable][p12]      |                  |                  | :material-check: | :material-check: |
| [Run: view runs and related data][p13]  | :material-check: | :material-check: | :material-check: | :material-check: |
| [Run: start a new run][p14]             |                  |                  | :material-check: | :material-check: |
| [Run: edit run info][p15]               |                  |                  | :material-check: | :material-check: |
| [Run: remove a run][p16]                |                  |                  | :material-check: | :material-check: |

Notes:

1. A guest can only access projects that have visibility set to `public`

[p1]: project.md#edit-project-info-and-settings
[p2]: project.md#remove-a-project
[p3]: code.md
[p4]: code.md#replace
[p5]: job/index.md#job-page
[p6]: job/create-job.md
[p7]: job/index.md#edit-job-info
[p8]: job/index.md#remove-a-job
[p9]: variable/index.md
[p10]: variable/index.md#web-interface
[p11]: variable/index.md#web-interface
[p12]: variable/index.md#web-interface
[p13]: run/index.md
[p14]: run/index.md#starting-a-run
[p15]: run/index.md#edit-run-info
[p16]: run/index.md#remove-a-run
