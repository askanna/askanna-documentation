# Variable

In AskAnna we add variables to every run environment. You can use variables for settings, passwords, tokens, etcetera.
In this document, you can find more information about how you can configure & manage variables and how you can use them
within the AskAnna run environment.

## Project & payload variables

Within AskAnna you can configure variables in different ways. The first method is that you can add variables on the
project page. `Project variables` are available in every run environment for runs linked to this project.

`Payload variables` are only available for a single run. AskAnna scans the first 10,000 characters of the JSON payload.
Every key becomes a variable name, and every value the value of that variable. See
["Jobs - Payload variables"](../job/create-job.md#payload-variables) for more information.

## Manage project variables

### Web interface

On the project page, you can find the section `VARIABLES`. Here you can do the following actions:

- Create a new variable
- Edit a variable
- Delete a variable

![Variable section](../media/variable/askanna-variable-page.png){ loading=lazy }

The maximum length for a variable name is **128 characters**. For a value, there is no technical limit.

### AskAnna CLI

Via [the AskAnna CLI](../cli.md) you can manage the variables as well.

#### List variables

```shell
askanna variable list

askanna variable list --project "{PROJECT_SUUID}"
```

#### Add variable

```shell
askanna variable add

askanna variable add --name "Variable name" --value "Variable value" --project "{PROJECT_SUUID}"
```

For all options, check the help: `askanna variable add --help`

#### Change variable

Change the value:

```shell
askanna variable change --id "{VARIABLE_SUUID}" --value "New variable value"
```

Or name:

```shell
askanna variable change --id "{VARIABLE_SUUID}" --name "New variable name"
```

For all options, check the help: `askanna variable change --help`

#### Delete variable

```shell
askanna variable delete --id "{VARIABLE_SUUID}"

askanna variable delete --id "{VARIABLE _SUUID}" --force
```

### Python SDK

The Python SDK has a variables module.
[See also the SDK documentation about project variables.](../python-sdk/variable.md)

## Masked variables

When you create or edit a variable, you see the option to mark the variable as masked. You can use masked variables for
sensitive information. The values are exposed in the run environment, but we will not reveal these values to
interfaces like the web interface. This way, you can not accidentally show sensitive information like a password, token
or other information your want to keep secret.

## How to use the variables

You can use variables in the job definition. Also, variables become available as environment variables so that you can
import them into your script.

### Use a variable in your script

In a Python script, I want to set up a connection to a database. Via the project variables, we have a secure way to add
and manage these settings. In the project variables we create the variables:

- POSTGRES_HOST
- POSTGRES_DATABASE
- POSTGRES_USER
- POSTGRES_PASSWORD

At least we advice that you create password kind of values as a masked variable. In the Python script, we can use the
project variables by importing them via environment variables:

```python
import os
import psycopg2

conn = psycopg2.connect(
    host=os.getenv("POSTGRES_HOST"),
    database=os.getenv("POSTGRES_DATABASE"),
    user=os.getenv("POSTGRES_USER"),
    password=os.getenv("POSTGRES_PASSWORD")
)
```

### Use a variable in the job definition

In a project, you have a job that trains a model. The artifact of this job contains the trained models, and you want to
use the latest approved trained models to serve a prediction model.

By creating a variable `TRAINED_MODEL_UUID` and give this model a value of the SUUID of the latest trained model run,
you can realize this scenario.

```yaml
train-model:
  job:
    - python src/train_model.py
  output:
    artifact:
      - models/

serve-model:
  job:
    - askanna artifact get -i ${TRAINED_MODEL_SUUID} -o models/artifact.zip
    - unzip -q -o models/artifact.zip
    - python src/serve_model.py
```

### Update the value via the AskAnna CLI

It's possible to update the value of a variable via the AskAnna CLI. The previous example mentioned that you could
create a variable with the SUUID of the latest trained model for the serve-model job.

We can extend the train-model job by an extra step to update this variable with the latest trained model. You can find
the unique identifier of the variable on the project variable page in the SUUID column. The variable `AA_RUN_SUUID` is
by default available in every run environment. See also
"[Job - AskAnna variables](../job/create-job.md#askanna-variables)".

```yaml
train-model:
  job:
    - python src/train_model.py
    - askanna variable change -i 1eAe-ssKI-v2Dq-CNYA -v ${AA_RUN_SUUID}
  output:
    artifact:
      - models/
```

### Overrule a project variable with a payload variable

As mentioned, the first 10,000 characters of the JSON payload are also made available as variables. This makes it
possible to configure project variables, which can be overruled via payload variables when you trigger a run.

For example, by default we want the job train-model to update the variable containing the latest SUUID. Via the
payload, we want to be able to overrule this default value.

On the project page, we create a variable `UPDATE_TRAINED_MODEL` with a value `True`. Next, we configure the
train-model with an optional argument to update the variable:

```yaml
train-model:
  job:
    - python src/train_model.py
    - if [ "${UPDATE_TRAINED_MODEL}" = "True" ] ; then askanna variable change -i 1eAe-ssKI-v2Dq-CNYA -v ${AA_RUN_SUUID} ; fi
  output:
    artifact:
      - models/
```

Now, if you want to train a model, but you don't want the variable updated afterwards, you can trigger the job with a
payload containing the variable `UPDATE_TRAINED_MODEL` set to `False`:

```json
{
  "UPDATE_TRAINED_MODEL": "False"
}
```

## Security

Variables can contain sensitive information. We cannot expose all security measures we take to protect your
information, but we can share some of them.

The first layer of protection is that you can configure that a variable value is masked. This means that for these
variables, we will not expose the value to the web interface.

Also, all information stored in our database is encrypted. Only when the data is exposed to an interface it will be
decrypted. If someone gets access to the database records, the person will get an encrypted record.
