# Schedule a Job

Sometimes you have a job that you want to run frequently. With AskAnna you can add a schedule to your
[askanna.yml](../code.md#askannayml) configuration. On this page we will tell you how you can create schedules for a
job and check the applied schedules for a job in the web interface.

## Create a Schedule

In the definition of the job, you can optionally define a schedule:

```yaml
name of the job:
  job:
    - ...
  schedule:
    - "@weekly"
```

Or multiple schedules:

```yaml
name of the job:
  job:
    - ...
  schedule:
    - "@weekly"
    - day: 1
    - "0 8 * * *"
```

### Schedule templates

In AskAnna we use cron to schedule jobs. Because cron syntax is not always easy to read, we added a couple of templates:

| Name      | Cron expression | Description                         |
| --------- | --------------- | ----------------------------------- |
| @midnight | `0 0 * * *`     | Every day at 12 AM                  |
| @yearly   | `0 0 1 1 *`     | The first day of the year at 12 AM  |
| @annually | `0 0 1 1 *`     | The first day of the year at 12 AM  |
| @monthly  | `0 0 1 * *`     | The first day of the month at 12 AM |
| @weekly   | `0 0 * * 0`     | Every week on Sunday at 12 AM       |
| @daily    | `0 0 * * *`     | Every day at 12 AM                  |
| @hourly   | `0 * * * *`     | Every hour at minute 0              |

### List using cron names

You can add a list with names that refer to the different [cron names](#cron-syntax):

```yaml
name of the job:
  job:
    - ...
  schedule:
    - minute: 10
      hour: 8
      day: "5-10"
      month: 12
      weekday: 1
```

The list example above translates into a schedule that runs at 08:10 on day-of-month from 5 through 10 and only on
Monday.

You don't have to specify every item in the list. The default values are:

| Name    | Default value | Description            |
| ------- | ------------- | ---------------------- |
| minute  | 0             | Ones, at minute 0      |
| hour    | 0             | Ones, at hour 0        |
| day     | \*            | Every day of the month |
| month   | \*            | Every month            |
| weekday | \*            | Every day of the week  |

For example, the following definition will run at 12 AM (00:00) on the first day of the month:

```yaml
name of the job:
  job:
    - ...
  schedule:
    - day: 1
```

### Cron syntax

We also support the official [cron syntax](https://en.wikipedia.org/wiki/Cron). Cron expressions looks like
`* * * * *`. The syntax has 5 positions. Each position has it's own value:

| Position | Name               | Allowed values                                    |
| -------- | ------------------ | ------------------------------------------------- |
| 1.       | Minute             | 0 - 59                                            |
| 2.       | Hour               | 0 - 23                                            |
| 3.       | Day (of the month) | 1 - 31                                            |
| 4.       | Month              | 1 - 12                                            |
| 5.       | Day of the week    | 0 - 6 (Sunday = 0, Monday = 1, ..., Saterday = 6) |

Supported syntax for Cron (and also for the [list using cron names](#list-using-cron-names) values):

| Value | Description                                                                                     |
| ----- | ----------------------------------------------------------------------------------------------- |
| \*    | any value, for example `day: *` will run on any day                                             |
| ,     | list seperator, for example `hour: "4,10,16"` wil run on 04:00, 10:00 an 16:00                  |
| -     | range of values, for example `hour: "8-17"` will run every hour between 08:00 and 17:00         |
| /     | ste value, for example `hour: */6` will run every 6th hour, so at 06:00, 12:00, 18:00 and 00:00 |

### Cron editor

To make it easier to define or check a cron schedule, you can use the quick and simple editor for cron schedule
expressions: [crontab guru](https://crontab.guru/)

## Time zone

When you set a default time zone for the project or a specific time zone for the job, the schedule will be set using
that time zone. An example job configuration with the time zone specified:

```yaml
name of the job:
  job:
    - ...
  schedule:
    - hour: 8
  timezone: Europe/Amsterdam
```

For more information about using time zone in jobs, see [Create a Job - Time zone](create-job.md#time-zone).

## Validate a Schedule

Since version 0.8.0 of the [AskAnna CLI](../cli.md), we also support validation of the `askanna.yml`. Part of the
validation is that we also validate the schedules.

Validation runs the moment you push code to your project in AskAnna. If you have specified an invalid schedule, the
AskAnna CLI prints feedback in the terminal, so you know what to fix.

## View active Schedules

When a job has a schedule, we also show it on the [job page](index.md#job-page). Here you can find the date-time of the
next run. Also, you can find the schedule or a list of schedules:

![Job with a schedule](../media/job/job-page-with-schedule.png){ loading=lazy }

In the web interface you can check the schedules in a human-friendly format. When you point your mouse on a schedule,
you can see the original definition and next run:

![Job schedule meta](../media/job/job-page-schedule-meta.png){ width=600px, loading=lazy }

## Runs triggered by a schedule

If a schedule triggers a run, this information is available. When you request the runinfo in the response, you will see
the trigger value set to `SCHEDULE`. This information is also shown on the run page:

![Run triggered by a schedule](../media/job/run-page-schedule.png){ loading=lazy }

In AskAnna we check every minute if we should trigger new runs. If somehow, for example a job was not triggered because
of a system restart or interruption, we will do a check every 5 minutes if we might have missed to trigger a new run.
If that's the case, we will make sure it is triggered on the next schedule.
