# Run

A central component in data science projects is the runs of a job. In AskAnna you can track all relevant information
related to a run. For example you can track input, metrics, parameters, code version, artifacts and results. If you
want to track something, and we currently don't support it, [contact (support@askanna.io)](mailto:support@askanna.io)
us.

The information you track in a run can be used for later comparison. In the phase of experimentation you can compare
runs and see which settings will perform the best. Or in production phase you can monitor the stability of the model
over time. Via AskAnna you can easily extract/query the project information and build your own dashboards to get
insights from your runs.

## Starting a run

Check the [job documentation](../job/index.md) to read about [How to run a job](../job/run-job.md). On that page you
can find the information about how to run a job using the:

- [Web interface](../job/run-job.md#web-interface)
- [Command line](../job/run-job.md#askanna-cli)
- [Python SDK](../job/run-job.md#askanna-sdk)

## Retrieving run data via the AskAnna CLI, SDK & API

The information that we show on the run page can always be retrieved via the AskAnna API and in most cases also
[the AskAnna CLI](../cli.md). The only relevant information you need to know is the SUUID of the run. You can find the
SUUID of the run on the [run page](run-page.md) or you can copy the SUUID from the
[run overview](run-overview.md).

### Get Payload

AskAnna CLI:

```shell
askanna-run-utils get-payload --run "{RUN_SUUID}"
```

Optionally you can also specify the output path via `--output`.

AskAnna API:

```shell
curl -X GET \
-H 'Authorization: Token {YOUR_TOKEN}' \
'https://beta-api.askanna.eu/v1/run/{RUN_SUUID}/payload/'
```

### Get Result

AskAnna CLI:

```shell
askanna result get --id "{RUN_SUUID}"
```

Python SDK:

```python
from askanna import result

result = result.get(run_suuid="{RUN_SUUID}")
```

AskAnna API:

```shell
curl -X GET \
-H 'Authorization: Token {YOUR_TOKEN}' \
'https://beta-api.askanna.eu/v1/result/{RUN_SUUID}/'
```

### Get Metrics

AskAnna SDK:

```python
from askanna import run

run_metrics = run.get_metric(run_suuid="{RUN_SUUID}")
```

AskAnna API:

```shell
curl -X GET \
-H 'Authorization: Token {YOUR_TOKEN}' \
'https://beta-api.askanna.eu/v1/run/{RUN_SUUID}/metric/'
```

### Get Artifact

AskAnna CLI:

```shell
askanna artifact get --id "{RUN_SUUID}"
```

Optionally you can also specify the output path via `--output`.

AskAnna API:

```shell
curl -X GET \
-H 'Authorization: Token {YOUR_TOKEN}' \
'https://beta-api.askanna.eu/v1/run/{RUN_SUUID}/artifact/'
```

### Get Variables

AskAnna SDK:

```python
from askanna import run

run_variable = run.get_variable(run_suuid="{RUN_SUUID}")
```

AskAnna API:

```shell
curl -X GET \
-H 'Authorization: Token {YOUR_TOKEN}' \
'https://beta-api.askanna.eu/v1/run/{RUN_SUUID}/variable/'
```

### Get Log

AskAnna CLI:

```shell
askanna run log --id "{RUN_SUUID}"
```

AskAnna SDK:

```python
from askanna import run

run_log = run.log(run_suuid="{RUN_SUUID}")
```

AskAnna API:

```shell
curl -X GET \
-H 'Authorization: Token {YOUR_TOKEN}' \
'https://beta-api.askanna.eu/v1/run/{RUN_SUUID}/log/'
```

## Run failures

It can happen that a run fails. When this happens, AskAnna will always try to save the latest log and the specified
artifact of the run. Because the run was not succesfull, we will not save a result for the run.

In case something went wrong with running your code, you can probably find information about the error in the run log.
Our jobs run in containers, and we keep them for 24 hours. So in case the logs doesn't help you to figure out what
went wrong, you can always [contact us (support@askanna.io)](mailto:support@askanna.io). With your permission we can
check the container used for the run and see if we can help you.

## Edit run info

AskAnna is designed to empower you to reproduce results. Most of the information of a run cannot be edited. Exception
is the name and description of the run. Via the web interface, you can edit the information of the run. In addition,
via the CLI and Python SDK, you can update the description and change the run's name as well.

### Web interface

On the run page you can click on :material-dots-vertical:. In this menu select `EDIT RUN INFO` and you can update the
run info. Here you can also add or update a description of the run. Everybody with access to the run can see this
information.

![AskAnna Run - Edit info](../media/run/askanna-run-edit-info.gif){ loading=lazy }

!!! tip "Markdown support"
    The text editor support Markdown instructions. These instructions will automatically transferred into formatted
    text.

### CLI

With the [AskAnna CLI](../cli.md) you can change the run info with your command line. In your terminal run:

```shell
askanna run change
```

Optionally you can also directly provide the info you want to change. For example, if you only want to change the
`name`, you don't have to specify the description. If you don't add it, it will not be updated.

```shell
askanna run change --id "{RUN_SUUID}" --name "New run name" --description "New run description"
```

### Python SDK

With the [AskAnna Python SDK](../python-sdk/index.md), you can change the run info using the SUUID of the run:

```python
from askanna import run

run.change(run_suuid="{JOB_SUUID}", name="New run name", description="New run description")
```

## Remove a run

You can remove a run via the web interface, CLI, Python SDK and API. When you remove a run, this is what happens:

- the run will be removed
- all data related to the run will be removed
- if other projects, systems or jobs use information or files from the run, they are not able to use it anymore

!!! warning "Recovery period of 30 days"
    When you remove a run, you will not have access to the run anymore. After 30 days, the system will
    entirely delete the run from the backend. Within 30 days, you can [contact us](../contact.md) if you want to
    undo the removal.

    If the run and related files should be removed as soon as possible, you can [contact us](../contact.md).

### Web interface

In the web interface you can remove a run from the run page by clicking on :material-dots-vertical: on the right side.
Next select `REMOVE THIS RUN` and confirm that you want to remove the run.

### Command line (CLI)

With the [AskAnna CLI](../cli.md) you can remove a run on the command line. In your terminal run:

```shell
askanna run remove --id "{RUN_SUUID}"
```

### Python SDK

With the [Python SDK](../python-sdk/index.md) you can delete a run via:

```python
from askanna import run

run.delete(run_suuid="{RUN_SUUID}")
```

### API

An authenticated user can DELETE a run via:

`DELETE https://beta-api.askanna.eu/v1/run/{RUN_SUUID}/`

If deleting a run was successful, AskAnna will respond with an HTTP-code 204.
