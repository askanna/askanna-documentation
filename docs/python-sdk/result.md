# Python SDK - Result

## askanna.result.get

```python
import askanna

askanna.result.get(run_suuid="{RUN_SUUID}")
```

<h3>Parameters</h3>

| Name      | Type   | Required | Description      |
| --------- | ------ | -------- | ---------------- |
| run_suuid | String | Yes      | SUUID of the run |

<h3>Output</h3>

Content of the result in bytes

## askanna.result.download

```python
import askanna

askanna.result.download(run_suuid="{RUN_SUUID}", output_path="{FILE_PATH}")
```

<h4>Parameters</h4>

| Name        | Type           | Required | Description                                      |
| ----------- | -------------- | -------- | ------------------------------------------------ |
| run_suuid   | String         | Yes      | SUUID of the run you want to get the result from |
| output_path | Path or String | Yes      | Path to save the result to.                      |

<h4>Output</h4>

The output is the result saved at the `output_path`. This function returns `None`.

## askanna.result.get_content_type

```python
import askanna

askanna.result.get_content_type(run_suuid="{RUN_SUUID}")
```

<h3>Parameters</h3>

| Name      | Type   | Required | Description      |
| --------- | ------ | -------- | ---------------- |
| run_suuid | String | Yes      | SUUID of the run |

<h3>Output</h3>

String

## askanna.result.get_filename

```python
import askanna

askanna.result.get_filename(run_suuid="{RUN_SUUID}")
```

<h3>Parameters</h3>

| Name      | Type   | Required | Description      |
| --------- | ------ | -------- | ---------------- |
| run_suuid | String | Yes      | SUUID of the run |

<h3>Output</h3>

String
