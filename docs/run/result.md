# Result of a Run

Optionally, in the [job definition](../job/create-job.md#job-definition) you can specify the result that should be
saved when you run the job:

```yaml
name of the job:
  job:
    - ...
  output:
    result: output/example.json
```

When saving the result, we try to autodetect the format. We currently implemented support for file types:

- JSON
- XML
- CSV
- PDF
- Excel (xlsx and xls)
- Txt
- Parquet
- HTML
- Images (png, jpg and jpeg)

## View result

On the run page, you see the section `RESULT`. If you click on this tab, you will find more information about the
result of the run. You can get the following information:

- If possible, a preview of the result
- Download file option because we cannot preview the file type (f.e. Excel and PDF files)
- In case of no result, a message: `There is no result available for this run.`

When we preview the file, we show the first 10,000 characters. Also, you have options to:

- Download the original file
- Copy the result
- Optionally: switch views

In case of a JSON result that contains less than 10,000 characters, we also have a pretty view of the file. This will
make it easier to read the file. The copy button will copy the file as displayed in the web interface.
You can also view the original `raw` format by switching the view in the menu:

![Example run JSON result](../media/run/askanna-run-result.gif){ loading=lazy }

## Get Result

### Web interface

In the web interface, you can click the tab `RESULT` on the run page and download the result file of the run.

### AskAnna CLI

With the [AskAnna CLI](../cli.md) you can get the result of a run via the command line. In your terminal run:

```shell
askanna result get
```

Also you can specify the output path:

```shell
askanna result get --id "{RUN_SUUID}" --output "~/askanna/result.json"
```

### Python SDK

With the [AskAnna Python SDK](../python-sdk/index.md), you can get the result using the SUUID of the run:

```python
from askanna import result

result = result.get(run_suuid="{RUN_SUUID}")
```

Via the SDK you can also request information about the content type of the result:

```python
from askanna import result

content_type = result.get_content_type(run_suuid="{RUN_SUUID}")
```

### AskAnna API

```shell
curl -X GET \
-H 'Authorization: Token {YOUR_TOKEN}' \
'https://beta-api.askanna.eu/v1/run/{RUN_SUUID}/result/'
```

As a response, you will get the content of the file. We support different file types for a result. In the header, we
add information about the content type of the result.
