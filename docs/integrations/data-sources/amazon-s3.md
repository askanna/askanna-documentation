# Amazon S3

The Amazon S3 integration shows how to set up a connection to the Amazon S3 object storage using Python. We
demonstrate how to download files from the storage and vice versa. Before your Python script can interact with the
Amazon S3 storage, we first need to get Amazon credentials.

## Get access keys for Amazon

To connect to Amazon S3 object storage, you need an `access key id` and `secret access key`.

### AWS CLI

If you have the [AWS CLI](http://aws.amazon.com/cli/){target=_blank}, you can use it to interactive create a
configuration file. In your terminal run:

```shell
aws configure
```

Follow the prompts. In the end, you can find your credentials in the file `~/.aws/credentials/`.

### AWS Console

For instructions about creating a user using the IAM Console, follow the steps on
[Creating IAM users][aws-user]{target=_blank}.

With an IAM user, you can get access keys by following the steps in [Managing access keys][aws-keys]{target=_blank}.

[aws-user]: https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html#id_users_create_console
[aws-keys]: https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html#Using_CreateAccessKey

## Python

In the example below, we show how to connect the Amazon S3 object storage using Python. We demonstrate how to download
files from the storage and vice versa. To do this, we need Python 3.7 or newer and install the following packages:

- [boto3][boto3]{target=_blank} (required to set up the connection)
- [python-dotenv](https://pypi.org/project/python-dotenv/){target=_blank}

We recommend using a [virtual environment][venv]{target=_blank} and adding the packages to a
[requirements.txt][requirements]{target=_blank} file. In this file, you can add the following:

```cfg
boto3          # tested with version 1.20.4
python-dotenv  # tested with version 0.18.0
```

[venv]: https://askanna.io/how-to/2021/virtual-environments/
[requirements]: https://askanna.io/how-to/2021/virtual-environments/#using-a-requirements-file
[boto3]: https://boto3.amazonaws.com/v1/documentation/api/latest/index.html

### Download from Amazon S3

See also [Downloading files][download-files]{target=_blank} in the [Boto3 documentation][boto3]{target=_blank}.

[download-files]: https://boto3.amazonaws.com/v1/documentation/api/latest/guide/s3-example-download-file.html

```python
import os
import boto3

# More about dotenv in the section `Configure dotenv`
from dotenv import find_dotenv, load_dotenv
load_dotenv(find_dotenv())


bucket_name = "S3 BUCKET NAME"      # Bucket to download from
source_object_name = "OBJECT NAME"  # S3 object name
target_file_name = "PATH TO FILE"   # File to save the object to

s3_client = boto3.client(
    "s3",
    aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
    aws_secret_access_key=os.getenv("AWS_SECRET_ACCESS_KEY"),
)

s3_client.download_file(bucket_name, source_object_name, target_file_name)
```

### Uploading to Amazon S3

See also [Uploading files][uploading-files]{target=_blank} in the [Boto3 documentation][boto3]{target=_blank}.

[uploading-files]: https://boto3.amazonaws.com/v1/documentation/api/latest/guide/s3-uploading-files.html

```python
import os
import boto3
from botocore.exceptions import ClientError

# More about dotenv in the section `Configure dotenv`
from dotenv import find_dotenv, load_dotenv
load_dotenv(find_dotenv())

bucket_name = "S3 BUCKET"        # Bucket to upload to
source_file_name = "PATH FILE"   # File to upload

s3_client = boto3.client(
    "s3",
    aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID"),
    aws_secret_access_key=os.getenv("AWS_SECRET_ACCESS_KEY"),
)

response = s3_client.upload_file(file_name, bucket_name)
```

### Configure dotenv

In the Python examples, we use:

```python
from dotenv import find_dotenv, load_dotenv
load_dotenv(find_dotenv())
```

These two lines make it possible to develop your Python code locally, while you can also run the same code in AskAnna.
When you [add project variables](#add-askanna-project-variables), these variables will become available as
environment variables in the run environment.

Locally, you can add a file `.env`. When you run the Python code locally, the environment variables are loaded from
this file. Read more about this on the project page of
[python-dotenv](https://pypi.org/project/python-dotenv/){target=_blank}.

To run the above example, you need a `.env` file with:

```cfg
AWS_ACCESS_KEY_ID={ACCESS_KEY}
AWS_SECRET_ACCESS_KEY={SECRET_KEY}
```

For how to get the values of these variables, see [Get access keys for Amazon](#get-access-keys-for-amazon).

### More examples

In the [Boto3 documentation][boto3]{target=_blank}, you can find more [Amazon S3 examples][s3-examples]{target=_blank}.

[s3-examples]: https://boto3.amazonaws.com/v1/documentation/api/latest/guide/s3-examples.html

## Add AskAnna project variables

To run the above examples as a [job](../../job/index.md) in AskAnna, you should add
[project variables](../../variable/index.md). On the [project page](../../project.md#project-page), go to the tab
variables. Here you can create new variables. To run the above example, you should add variables with names and
corresponding values:

1. AWS_ACCESS_KEY_ID
2. AWS_SECRET_ACCESS_KEY

For how to get the values of these variables, see [Get access keys for Amazon](#get-access-keys-for-amazon).

!!! warning
    Make sure to set the variables to **masked**. You don't want to expose these values.
