# Workspace

An AskAnna workspace is the place where you can collaborate on your data science projects. A workspace can be used by
an organization, a team, individual persons, etc. Within a workspace you can find your [projects](project.md).
Also you can manage the [people](#workspace-members) active in the workspace or other workspace admin related tasks.

![Workspace](media/workspace/askanna-workspace.png)

In a workspace you can find all projects that are linked to the workspace. Projects are listed as cards showing the
project `name` and `description`. When you click on a card, the project page opens.

Besides viewing all workspace projects, there is more on the workspace page. In the gray bar you can:

- see the name of the workspace
- [create a project](project.md#create-a-project)
- :material-filter-outline:: shows that no search or filter options are active. Click on it to open/hide the search,
  filter and sort options.
- :material-filter:: shows that a search or filter is active. Click on it to open/hide the search, filter and sort
  options.
- :octicons-chevron-down-16:/:octicons-chevron-up-16:: show/hide the workspace meta information. The information
  contains the workspace SUUID, creation time, creator and description.
- :material-dots-vertical:: get more workspace options

## Menu bar

On top of the screen you can find the AskAnna menu bar. When you click on the AskAnna logo, you will go back to the
workspace "home" page.

If you click on **workspaces** or **projects** a menu will open. In this menu you can see all workspaces/projects you
have access too. Additionally you can explore public workspaces and projects. Click on a workspace or project to open
it.

On the right side you find an avatar or your profile image. If you click on the avatar you get another menu with a
link to edit your profile, sign out and a reference to the documentation.

## Workspace members

In the gray workspace bar click on :material-dots-vertical: and click on People. On the people page, you can see
members active in the workspace. With workspace members you can collaborate on your projects.

![People page](media/workspace/askanna-people.gif)

### Roles

Within AskAnna we have three workspace roles:

1. Workspace admin
2. Workspace member
3. Workspace viewer

On the page [permissions and roles](permissions.md#workspace-membership), you can find more information about what
each role can do.

### Invite people

When you click on `invite more people`, you will get a form where you can enter the email addresses of the people you
want to invite. Also, you can select the [role](#roles) the invited person(s) should get. By default you invite people
with the role `Workspace member`.

![Invite people](media/workspace/askanna-people-invite.gif)

When you are done entering email addresses, you can send the invites. They will receive an email with instructions on
how to join and a unique link to join your workspace. They can use an existing account, or create a new account to join
your workspace.

If you entered the email addresses of existing members, you cannot invite them a second time. You will see an error
message. You can click on the error button to remove them. Also, if you entered an invalid email address, you could use
the related button to remove them.

### Filter & sort

It's possible to sort the list or the filter on account type or status.

![Filter and sort People](media/workspace/askanna-people-filter-sort.gif)

## Workspace visibility

The workspace visibility can be set to [Private](#private) or [Public](#public). In
[Edit workspace info and settings](#edit-workspace-info-and-settings) you can find how to change this setting.

### Private

By default workspace visibility is set to `Private`. When the workspace visibility is set to private, this means
that only [workspace members](#workspace-members) can open the workspace and projects within this workspace.

### Public

You can also set the workspace visiblity to `Public`. This means that the workspace name and description becomes
visible for everyone, also people without an account in AskAnna.

Additional, if the workspace visibility is set to `Public`, you can also make projects publicly visible. By doing this
you can open your project and share it with the world.

## Create a workspace

You can create an AskAnna workspace via the web interface, CLI, Python SDK and API. For every method, the workspace
information in the table below is relevant when creating a workspace:

| Attribute   | Required | Description                                                              |
| ----------- | -------- | ------------------------------------------------------------------------ |
| Name        | Yes      | The name of the workspace                                                |
| Description | No       | A description of the workspace                                           |
| Visibility  | No       | By default the visibility is `PRIVATE`, it could also be set to `PUBLIC` |

### Web interface

You can create a workspace via the menu `WORKSPACES` or by going to the explore workspaces page and click on
++"\+ CREATE WORKSPACE"++. You can fill in a workspace name which will directly create the workspace or you can choose
for ++"MORE OPTIONS"++ where you can also fill in the workspace description and visiblity.

![Create workspace](media/workspace/askanna-create-workspace.png)

### Command line (CLI)

With the [AskAnna CLI](cli.md) you can create a new workspace info with your command line. In your terminal run:

```shell
askanna workspace create

askanna workspace create --name "Workspace name"
```

For all options, check the help: `askanna workspace create --help`

### Python SDK

With the [Python SDK](python-sdk/workspace.md#askannaworkspacecreate) you can create a workspace via:

```python
from askanna import workspace

workspace = workspace.create(name="Workspace name")
```

### API

An authenticated user can POST a request for creating a new workspace via:

`POST https://beta-api.askanna.eu/v1/workspace/`

The JSON body should contain at least the workspace name:

```json
{
    "name": "Workspace name",
    "description": "Workspace description (optional)",
    "visibility": "PRIVATE",  # (or "PUBLIC")
}
```

If creating a workspace via the post was successful, AskAnna will respond with an HTTP-code 201 and will give back the
workspace information.

## Edit workspace info and settings

Via the web interface, CLI, Python SDK and API, you can edit the information and settings of the workspace. Although
you can provide a description of the workspace, we don't display this information at the moment.

### Web interface

On the workspace home you can click on :material-dots-vertical:. In this menu select `Edit workspace` and you can
update the workspace info and settings.

![Edit workspace](media/workspace/edit-workspace-info.gif){ loading=lazy }

!!! tip "Markdown support"
    The text editor support Markdown instructions. These instructions will automatically transferred into formatted
    text.

### Command line (CLI)

With the [AskAnna CLI](cli.md) you can change the workspace info with your command line. In your terminal run:

```shell
askanna workspace change
```

Optionally, you can also directly provide the info you want to change:

```shell
askanna workspace change --id "{WORKSPACE_SUUID}" --name "New workspace name"
```

### Python SDK

With the [Python SDK](python-sdk/workspace.md#askannaworkspacechange) you can change workspace info via:

```python
from askanna import workspace

workspace = workspace.change(workspace_suuid="{WORKSPACE_SUUID}", name="New workspace name")
```

### API

An authenticated user can PATCH a workspace via:

`PATCH https://beta-api.askanna.eu/v1/workspace/{WORKSPACE_SUUID}/`

The JSON body should contain the fields you want to update:

```json
{
    "name": "New workspace name",
    "description": "New workspace description",
    "visibility": "PRIVATE",  # (or "PUBLIC")
}
```

If changing a workspace was successful, AskAnna will respond with an HTTP-code 200 and will give back the updated
workspace information.

## Remove a workspace

You can remove an AskAnna workspace via the web interface, CLI, Python SDK and API. When you remove a workspace, this
is what happens:

- all data related to the AskAnna workspace will be removed
- if other workspaces in AskAnna or external systems use projects, jobs, files or other parts of the AskAnna
  workspace, they are not able to use it anymore

!!! warning "Recovery period of 30 days"
    When you remove a workspace, you will not have access to the workspace anymore. After 30 days, the system will
    entirely delete the workspace from the backend. Within 30 days, you can [contact us](contact.md) if you want to
    undo the removal.

    If the workspace and related files should be removed as soon as possible, you can [contact us](contact.md).

### Web interface

In the web interface you can remove a workspace via the workspace home page or on the workspace list. Click on
:material-dots-vertical: at the right side of the page or card. Then click on `Remove workspace`.

### Command line (CLI)

With the [AskAnna CLI](cli.md) you can remove a workspace on the command line. In your terminal run:

```shell
askanna workspace remove --id "{WORKSPACESUUID}"
```

### Python SDK

With the [Python SDK](python-sdk/workspace.md#askannaworkspacedelete) you can delete a workspace via:

```python
from askanna import workspace

workspace.delete(workspace_suuid="{WORKSPACE_SUUID}")
```

### API

An authenticated user can DELETE a workspace via:

`DELETE https://beta-api.askanna.eu/v1/workspace/{WORKSPACE_SUUID}/`

If deleting a workspace was successful, AskAnna will respond with an HTTP-code 204.
