# Create a Job

You create a job via a manifest that defines the job’s behavior. You can add an [askanna.yml](../code.md#askannayml)
file to your project code. In the [askanna.yml](../code.md#askannayml) file you can specify a job. After specifying the
job, you can push/upload your code to AskAnna and AskAnna will create the job.

## Job definition

An example of a job definition:

```yaml
name of the job:
  job:
    - pip install -r requirements.txt
    - python train_your_model.py --input_file ${AA_PAYLOAD_PATH}
  output:
    result: models/model_summary.json
    artifact:
      - models/
```

### name of the job (required)

The job definition starts with the name of the job. You cannot use the following names as a job name:

- push-target
- environment
- cluster
- image
- worker
- variables

### job (required)

Here you list the commands that you want to execute in the run. In the above example, the first command executed is
installing the requirements. If that command finished successful, we will also run a second command that triggers a
Python script.

### output

In the `output` you can define which files you want to store. There are two options:

1. result
1. artifact

In `result` you can refer to a single file that contains the result of the run. For example, a JSON file with the
summary of a train job. Or the prediction served by a predict job.

In `artifact` you can list directories or files that you want to save as artifact of the run.

## Environment

In the [askanna.yml](../code.md#askannayml) file you can specify the default environment for the project, or a specific
environment image that should be used for the job:

```yaml
name of the job:
  environment:
    image: askanna/python:3.11
  job:
    - ...
```

[Read more about 'Environments'](../environment/index.md)

## Notifications

It's possible to receive notifications for a job:

```yaml
name of the job:
  job:
    - ...
  notifications:
    all:
      email:
        - info@example.com
    error:
      email:
        - error@example.com
```

[Read more about 'Notifications'](notifications.md)

## Schedule

It's possible to schedule a job:

```yaml
name of the job:
  job:
    - ...
  schedule:
    - "@weekly"

```

[Read more about 'Scheduling a Job'](schedules.md)

## Use variables

In the job definition, you can also use variables. In the job example above the `${AA_PAYLOAD_PATH}` is a variable.
A variable can be used as a parameter in the job manifest and is also available as environment variables in the run
environment. Variables are created on different levels:

- [AskAnna variables](#askanna-variables)
- [Project variables](#project-variables)
- [Payload variables](#payload-variables)

In the job definition you can refer to the different variables via `${VARIABLE_NAME}`.

### AskAnna variables

AskAnna variables are always available in every job. The list of AskAnna variables:

| Name             | Description                                                                                 |
| ---------------- | ------------------------------------------------------------------------------------------- |
| AA_RUN_SUUID     | The unique identifier for the run                                                           |
| AA_JOB_NAME      | The name of the job                                                                         |
| AA_PROJECT_SUUID | The unique identifier for the project                                                       |
| AA_PACKAGE_SUUID | The unique identifier for the code package used                                             |
| AA_RESULT_SUUID  | The unique identifier for the endpoint to save the result of a run (will be removed "soon") |
| AA_TOKEN         | Token used to trigger the job                                                               |
| AA_REMOTE        | The URL of the AskAnna backend                                                              |

#### Payload

When you [run a job](run-job.md), it’s possible to send a JSON data body. This JSON file is made available in the
run environment. You can get the location of the payload file via the variable `AA_PAYLOAD_PATH`. This variable
is also set as an environment variable.

| Name             | Description                                  |
| ---------------- | -------------------------------------------- |
| AA_PAYLOAD_PATH  | The path of the payload file                 |
| AA_PAYLOAD_SUUID | The unique identifier for the payload        |

### Project variables

Project variables are variables defined on project level. On the project page you can find the section
[variables](../variable/index.md#manage-project-variables). Here you can manage variables. The project variables are
available via the name you defined and for both the job definition, as also as environment variables in the run
environment.

For more information check the [variables documentation](../variable/index.md).

### Payload variables

There are two use cases for adding a JSON file to a request. The first is adding data that can be used in the run.
Another use case is that the JSON body contains variables that can be used in the run.

AskAnna scans the first 10,000 characters of the payload JSON file. Every key will become a variable name, and every
value the value of that variable. In case the `Payload variable` and `Project variable` has the same name, AskAnna
will use the value of the `Payload variable`.

Example of a JSON payload:

```json
{
  "FROM_DATE": "2020-09-07",
  "UNTIL_DATE": "2020-09-11",
  "TRAINED_MODEL_ID": "k68o-Mi2L-mYte-7E54"
}
```

In the job definition you can use these JSON keys as variables:

```yaml
name of the job:
  job:
    - askanna artifact get --id ${TRAINED_MODEL_ID} --output "models/"
    - pip install -r requirements.txt
    - python your_model.py --from_date ${FROM_DATE} --until_date ${UNTIL_DATE}
```

In the above example, we use `Payload variables` to define which trained model and period we want to use to run the
model. With this setup you can easily switch between models and periods.

## Time zone

The AskAnna platform runs by default on servers that are configured to run on time zone `UTC`. However, if you want to
run a job in a time zone of your choice, then you can configure this.

```yaml
name of the job:
  job:
    - ...
  timezone: Europe/Amsterdam
```

Also, you can set the default time zone for all jobs:

```yaml
push-target: ...
timezone: Europe/Amsterdam

name of the job:
  job:
    - ...
```

If you set both the time zone for all jobs, and a different time zone for the job, then the latest will applied when
you run that job.

For time zones, we use the [tz database](https://en.wikipedia.org/wiki/Tz_database). In the `askanna.yml` config, you
can refer to any TZ database name from the list of tz database time zones:

[Open the list of TZ database time
zones](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones){ .md-button .md-button--primary }

When you run a job where the time zone is set, the time zone is set to the specified time zone at the startup of the
worker. So, for example, if you now run the `date` command in a job, you will see the date and time of the specified
time zone. We also set the time zone as environment variable `TZ` in the run environment.
