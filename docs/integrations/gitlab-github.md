# Deploy code via GitLab or GitHub

If your team already uses [GitLab](https://gitlab.com) or [GitHub](https://github.com) for managing code, you
might be interested in setting up a deploy job that automatically pushes code. The example code below is that when you
commit to the main branch, a GitLab CI job or GitHub Action pushes that version of the code to AskAnna.

The project on GitLab/GitHub should contain an [askanna.yml](../code.md#askannayml) file with the push-target set. This
is similar to the configuration you need to push the code from your local machine. On
[push code](../code.md#push-code) you can read more.

To authenticate, you use the token of a workspace member. This can be your token, but you can also add a new workspace
member to use for this kind of authentication. For example, at AskAnna we use the member `Mr. Robot`.

If you [installed the AskAnna CLI via pip](../cli.md#install) and logged in, you can find the token in the local
AskAnna configuration file. On Mac/Linux you can view the file in a terminal via:

```shell
cat ~/.askanna.yml
```

On Windows, you can find the `.askanna.yml` configuration file in your user home directory. You need the token in the
project configuration of GitLab/GitHub.

Next, you will read for each platform how to configure the autopush to AskAnna.

## GitLab CI

### Add project variable

First, add the token as a variable to the GitLab project. You can go through the following steps or read more about it
on the [GitLab documentation](https://docs.gitlab.com/ee/ci/variable/#add-a-cicd-variable-to-a-project).

- Open the GitLab project
- Hoover on `Settings` (left side menu bar)
- Select `CI/CD`
- Expand the `Variables` section
- Click `Add Variable` and add a variable using the following details:
    - Key: `AA_TOKEN`
    - Value: the copied token
    - Type: `Variable`
    - Environment scope: `All`
    - Flag `Mask variable`

### Config .gitlab-ci.yml

In the GitLab project code, you can add a file `.gitlab-ci.yml` or edit an existing version of the file. Add the next
GitLab CI job to the file. This job installs AskAnna and pushes the latest version of the code when there is a new
commit in the main branch.

```yaml
# This is a GitLab job to push code to AskAnna
Update AskAnna project:
  stage: deploy
  image: python:3-alpine
  script:
    - apk add git
    - python -m pip install --upgrade pip
    - pip install askanna
    - askanna push --force
  only:
    - main
```

That's it for GitLab. If you set up the above configuration, every time there is a commit to the main branch, it will
trigger a CI/CD job to push the code to the AskAnna project.

## GitHub Action

### Add project secret

Similar to GitLab, let's first add the token as a secret to the project. To add the token in GitHub, we need to set a
secret for the project following the next steps. Or read more about it in the
[GitHub documentation](https://docs.github.com/en/actions/reference/encrypted-secrets#creating-encrypted-secrets-for-a-repository).

- Open the GitHub project
- Click on `Settings`
- On the left side menu select `Secrets`
- Click on `New repository secret`
- Add a new secret with:
    - Name: `AA_TOKEN`
    - Value: the copied token

### Add workflow file

In the project code, you can add a GitHub workflow file: `.github/workflows/update-askanna.yml`

In this file, you can configure the job to push code to AskAnna. Copy-paste the following lines to the workflow file:

```yaml
# This is a GitHub workflow to push code to AskAnna
name: Update AskAnna project

on:
  push:
    branches: [ main ]

jobs:
  deploy:
    runs-on: ubuntu-latest

    steps:
      - uses: actions/checkout@v2
      - name: Set up Python
        uses: actions/setup-python@v2
        with:
          python-version: '3.9'
      - name: Install dependencies
        run: |
          python -m pip install --upgrade pip
          pip install askanna
      - name: Push code to the AskAnna project
        env:
          AA_TOKEN: ${{ "{{ secrets.AA_TOKEN " }}}}
        run: askanna push --force
```

That's it for GitHub. If you set up the above configuration, every time there is a commit to the main branch, it will
trigger a GitHub action to push the code to the AskAnna project.
