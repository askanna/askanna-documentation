# Tracking

## Track metric

### askanna.track_metric

```python
from askanna import track_metric

track_metric(name, value, label)
```

<h4>Parameters</h4>

| Name  | Type     | Required | Description                                                                           |
| ----- | -------- | -------- | ------------------------------------------------------------------------------------- |
| name  | String   | Yes      | Name of the value you want to track                                                   |
| value | <ul><li>String</li><li>Integer</li><li>Float/numeric</li><li>Date</li><li>Time</li><li>Datetime ([ISO8601](https://en.wikipedia.org/wiki/ISO_8601))</li><li>Boolean</li><li>Tag</li><li>Dictionary</li></ul> | Yes      | Value you want to track. See the list of types for formats we support in AskAnna. |
| label | <ul><li>List</li><li>Dictionary</li></ul> | No       | Labels you want to add to the tracked metric. Labels can only contain a name or a name & value. |

<h4>Output</h4>

No output. Track metric will temporarely store metrics in a local JSON file. When you run the script locally without a
run SUUID set, AskAnna will display the location of the local file.

### askanna.track_metrics

```python
from askanna import track_metrics

track_metrics(metrics, label)
```

<h4>Parameters</h4>

| Name    | Type       | Required | Description                                                                       |
| ------- | --------   | -------- | --------------------------------------------------------------------------------- |
| metrics | Dictionary | Yes      | Dictionary with metrics (name & values) you want to store. See [track_metric](#askannatrack_metric) for value types that we support. |
| label   | <ul><li>List</li><li>Dictionary</li></ul> | No       | Labels you want to add to the tracked metric. Labels can only contain a name or a name & value. |

<h4>Output</h4>

No output. Track metric will temporarely store metrics in a local JSON file. When you run the script locally without a
run SUUID set, AskAnna will display the location of the local file.

## Track variable

### askanna.track_variable

```python
from askanna import track_variable

track_variable(name, value, label)
```

<h4>Parameters</h4>

| Name  | Type     | Required | Description                                                                           |
| ----- | -------- | -------- | ------------------------------------------------------------------------------------- |
| name  | String   | Yes      | Name of the value you want to track                                                   |
| value | <ul><li>String</li><li>Integer</li><li>Float/numeric</li><li>Date</li><li>Time</li><li>Datetime ([ISO8601](https://en.wikipedia.org/wiki/ISO_8601))</li><li>Boolean</li><li>Tag</li><li>Dictionary</li></ul> | Yes      | Value you want to track. See the list of types for formats we support in AskAnna. |
| label | <ul><li>List</li><li>Dictionary</li></ul> | No       | Labels you want to add to the tracked variable. Labels can contain a name or a name & value. |

<h4>Output</h4>

No output. Track variable will temporarely store tracked variables in a local JSON file. When you run the script
locally without a run SUUID set, AskAnna will display the location of the local file.

### askanna.track_variables

```python
from askanna import track_variables

track_variables(variables, label)
```

<h4>Parameters</h4>

| Name      | Type       | Required | Description                                                                     |
| --------- | ---------- | -------- | ------------------------------------------------------------------------------- |
| variables | Dictionary | Yes      | Dictionary with variables (name & values) you want to store. See [track_variable](#askannatrack_variable) for value types that we support. |
| label     | <ul><li>List</li><li>Dictionary</li></ul> | No       | Labels you want to add to the tracked variables. Labels can contain a name or a name & value. |

<h4>Output</h4>

No output. Track variable will temporarely store variables in a local JSON file. When you run the script locally
without a run SUUID set, AskAnna will display the location of the local file.
