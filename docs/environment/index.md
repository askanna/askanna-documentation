# Environment

In AskAnna, we run jobs using container engines. This means that we can run most of the images published on
[Docker Hub](https://hub.docker.com/). Also, you can access private registries so that you can use custom images as
well.

In the [askanna.yml](../code.md#askannayml) you can specify the environment that the project or job should use.

## Configure environment

### Public image

For example, when you want to use an image from [Docker Hub](https://hub.docker.com/), you can specify the image you
want to use.

To set the default image for the project, specify the environment in the [askanna.yml](../code.md#askannayml):

```yaml
push-target: ...

environment:
  image: {IMAGE_NAME}
```

For the image name, you can provide the name. This will pull the image with tag `latest`. Optionally, you can
specify the image tag:

```yaml
environment:
  image: {IMAGE_NAME}:{IMAGE_TAG}
```

The above examples pull the image from [Docker Hub](https://hub.docker.com/). You can also pull an image from
another registry by providing the full URL.

```yaml
environment:
  image: registry.gitlab.com/askanna/demo/{IMAGE_NAME}:{IMAGE_TAG}
```

If you want to use an image specifically for a job, you can also specify the environment within the job configuration:

```yaml
name of the job:
  environment:
    image: {IMAGE_NAME}:{IMAGE_TAG}
  job:
    - ...
```

### Private image

We also support private registries that you can access with a username and password:

```yaml
environment:  
  image: {IMAGE_NAME}:{IMAGE_TAG}
  credentials:
    username: {IMAGE_REGISTRY_USERNAME}
    password: {IMAGE_REGISTRY_PASSWORD}
```

We strongly advise not to add sensitive information like a password in the [askanna.yml](../code.md#askannayml) file.
The best way is to save the password as a [project variable](../variable/index.md). You can refer to the variable
in the [askanna.yml](../code.md#askannayml) config.

Using the example above, you could add a masked project variable `IMAGE_PASSWORD`. This way, sensitive information
does not get exposed to everyone with access to the project code.

### GitLab registry

To access the GitLab registry, you need to have a
[deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html),
[group deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html#group-deploy-token) or
[personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). We prefer the deploy
token because it gives only access to the specific project.

#### Create deploy token

1. Sign in to your GitLab account
2. Go to the GitLab project you want to create a deploy token for
3. Go to **Settings > Repository**
4. Expand the **Deploy tokens** section
5. Set a name, expiry date (optional), and username (optional) for the token
6. Choose scope **read_registry**
7. Click **Create deploy token**
8. Save the deploy token as a [project variable](../variable/index.md) in AskAnna. After you leave or refresh the
   page, you can’t reaccess the token.
9. Save the username as a [project variable](../variable/index.md) or in the [askanna.yml](../code.md#askannayml)
   configuration.

#### Get GitLab image name

The GitLab image registry URL and name can be found via:

1. Go to your GitLab project or group
2. Go to **Packages & Registries > Container Registry**
3. Find the image you want to use, and copy the image name using the copy button

#### Add GitLab image in askanna.yml

Now you have all the information, to configure the environment in the [askanna.yml](../code.md#askannayml) file:

```yaml
environment:  
  image: ${GITLAB IMAGE URL + NAME}
  credentials:
    username: ${GITLAB USERNAME}
    password: ${GITLAB TOKEN}
```

### GitHub container

To access GitHub packages, you need to have an authentication token. With the image registry URL, image name and
GitHub token, you can set up the project configuration in AskAnna.

<!-- markdownlint-disable MD013 -->
!!! warning "At GitHub, you can only configure a personal access token. This means that everyone can use this token to access all the GitHub packages you have access too. Please keep this in mind."
<!-- markdownlint-enable MD013 -->

#### Create GitHub token

1. On [GitHub.com](http://github.com/) in the upper-right corner, click the profile photo
2. Go to **Settings**
3. In the left sidebar, go to **Developer settings**
4. In the left sidebar, go to **Personal access tokens**
5. Click **Generate new token**
6. Give your token a descriptive note
7. Choose scope **read:packages**
8. Click **Generate token**
9. Save the personal access token as a [project variable](../variable/index.md) in AskAnna. After you leave or
   refresh the page, you can’t reaccess the token.

#### Get GitHub image name

GitHub container images are linked to the GitHub project.

1. Go to the GitHub project
2. In the right sidebar, go to **Packages**
3. Find the image you want to use, and copy the image URL

#### Add GitLab image in askanna.yml

Now you have all the information, to configure the environment in the [askanna.yml](../code.md#askannayml) file:

```yaml
environment:  
  image: ${GITHUB IMAGE URL + NAME}
  credentials:
    username: ${GITHUB USERNAME}
    password: ${GITHUB TOKEN}
```

## AskAnna dependencies

To run a job in AskAnna, we need to have some tools available in the run environment. When you start a job, we will
automatically check if the following packages are installed, and if not we will try to install them:

- python3
- python3-pip
- curl
- git
- unzip
- tzdata

Also, we try to install our Python package [askanna](https://pypi.org/project/askanna/). If we cannot install a
dependency, the run will fail and in the run log you can find more information on why it failed.

## Default environment

The default environment is
[AskAnna Python 3.7](https://hub.docker.com/r/askanna/python/tags?page=1&ordering=last_updated&name=3.7). Besides
Python 3.7, we also installed:

- curl
- g++
- git
- tzdata
- unzip
- AskAnna CLI & SDK

You can find AskAnna environment images on [Docker Hub > AskAnna](https://hub.docker.com/u/askanna). For example, the
above installation with Python is also available for Python 3, Python 3.7, Python 3.8, Python 3.9, Python 3.10 and
Python 3.11. The image names you can use are:

- `askanna/python:3`
- `askanna/python:3.7`
- `askanna/python:3.8`
- `askanna/python:3.9`
- `askanna/python:3.10`
- `askanna/python:3.11`

## Cache

We cache images in AskAnna. The first time you run an image, it can take some time to run the specific job. AskAnna
need some time to install [the dependencies](#askanna-dependencies). After the dependencies are installed, we cache
that version of the image. The next time the job runs, you will see a shorter duration.

Before running the job, we will always check if the project should have access to the image. For example, in case of
a private image, the credentials should be valid. Using the image's digest, we also check if a newer version is
available for the configured image. If yes, we will pull that image.

Caching makes runnings jobs in AskAnna faster. Every weekend we run a set of jobs so that we can cache populair images.
Currently the images we pull and cache are based on:

- [AskAnna Python](https://hub.docker.com/r/askanna/python)
- [Python](https://hub.docker.com/_/python)
- [R Base](https://hub.docker.com/_/r-base)
- [Julia](https://hub.docker.com/_/julia)
- [TensorFlow](https://hub.docker.com/r/tensorflow/tensorflow)
- [PyTorch](https://hub.docker.com/r/pytorch/pytorch)
- [Hugging Face](https://hub.docker.com/u/huggingface)

We are not pulling all image tags for the above Docker projects. For the specific set of images we pull weekly, check
[this askanna.yml](https://gitlab.com/askanna/cache-environment-images/-/blob/main/askanna.yml).
