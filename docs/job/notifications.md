# Notifications

Sometimes you want to receive a notification when a job started, completed or failed. For example, when an external
system uses the [API](run-job.md#api) to trigger a run. Or when you frequently run a job via a
[schedule](schedules.md).

To enable notifications, you can add the configuration in the project [askanna.yml](../code.md#askannayml).
Currently, notifications are limited to email only.

## Notification groups

You can specify two groups who receive an email. In [notification types](#notification-types), you can
find more information about which kind of notification each group gets.

The notification groups are:

- All
- Error

## Notification types

In the table below you find the type of notifiations and information about which kind of notification each
notification group receives.

| Notification type | Group: **All**   | Group: **Error** |
| ----------------- | ---------------- | ---------------- |
| Start run         | :material-check: | :material-minus: |
| Completed run     | :material-check: | :material-minus: |
| Failed run        | :material-check: | :material-check: |

## Job notification

In the [askanna.yml](../code.md#askannayml), you can specify the notifications per group. You can add multiple email
addresses.

```yaml
name of the job:
  job:
    - ...
  notifications:
    all:
      email:
        - hello@example.com
        - email@example.com
    error:
      email:
        - error@example.com
```

## Notifications for all project's jobs

When notifications should be sent for all jobs, you don't have to repeat yourself to add the same config on
each job. You can config these receivers as a global element in the project [askanna.yml](../code.md#askannayml).

```yaml
push-target: ...

notifications:
  all:
    email:
      - hello@example.com

name of the job:
  job:
    ...
```

!!! info
    If you specify a global project notification group and a specific notification group for a job, then the global
    project and job receivers are combined. Everyone will receive the notifications.

## Use workspace groups

You can configure that all workspace admins or workspace members will receive the notification emails of a project:

```yaml
notifications:
  all:
    email:
      - workspace members
  error:
    email:
      - workspace admins
```

## Use project variables

It's possible to use a [project or payload variable](../variable/index.md) to configure the email addresses that
should get a notification. For the example below, you need to add a project or payload variable with the name
`a_project_variable`. The value of this variable should contain a **comma-separated list of email addresses**.

```yaml
notifications:
  error:
    email:
      - ${a_project_variable}
```

## View job notifications

In the web interface on the [job page](index.md#job-page), you can see if for the job notifications are configured.
If it's configured, you can click on it to view the configuration.

![View job notifications](../media/job/notifications-view.png){ loading=lazy }
