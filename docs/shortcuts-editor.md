# Shortcuts for the editor

We have designed our editor in such a way that you can use your mouse, keyboard shortcuts or Markdown to format your
text. You work with the editor when you create a new project, start a new run via the web interface. Or when you edit
a description of a workspace, project, job or run.

When you edit your text, you can also use shortcuts. Following, you find a list of keyboard and Markdown shortcuts we
support.

## Text formatting & shortcuts

| Action                    | Windows/Linux    | macOS           | Markdown                                           |
| ------------------------- | ---------------- | --------------- | -------------------------------------------------- |
| Bold                      | ++ctrl+b++       | ++cmd+b++       | `**bold**` or `__bold__`                           |
| Italic                    | ++ctrl+i++       | ++cmd+i++       | `*italic*` or `_italic_`                            |
| Underline                 | ++ctrl+u++       | ++cmd+u++       |                                                    |
| Strikethrough             | ++ctrl+shift+x++ | ++cmd+shift+x++ | `~~strikethrough~~`                                |
| Highlight                 | ++ctrl+shift+h++ | ++cmd+shift+h++ | `==highlight==`                                    |
|                           |                  |                 |                                                    |
| Header 1                  | ++ctrl+alt+1++   | ++cmd+alt+1++   | `# Header 1`                                       |
| Header 2                  | ++ctrl+alt+2++   | ++cmd+alt+2++   | `## Header 2`                                      |
| Header 3                  | ++ctrl+alt+3++   | ++cmd+alt+3++   | `### Header 3`                                     |
| Header 4                  | ++ctrl+alt+4++   | ++cmd+alt+4++   | `#### Header 4`                                    |
| Header 5                  | ++ctrl+alt+5++   | ++cmd+alt+5++   | `##### Header 5`                                   |
| Header 6                  | ++ctrl+alt+6++   | ++cmd+alt+6++   | `###### Header 6`                                  |
|                           |                  |                 |                                                    |
| Bullet list               | ++ctrl+shift+8++ | ++cmd+shift+8++ | `* bullet list`<br>or `- bullet list`              |
| Numeric list              | ++ctrl+shift+7++ | ++cmd+shift+7++ | `1. numeric list`                                  |
| Task list                 | ++ctrl+shift+9++ | ++cmd+shift+9++ | `[ ] Open task`<br>or `[x] Completed task`         |
|                           |                  |                 |                                                    |
| Horizontal divider        |                  |                 | `---`                                              |
|                           |                  |                 |                                                    |
| Quote                     | ++ctrl+shift+b++ | ++cmd+shift+b++ | `> quote`                                          |
| Code                      | ++ctrl+e++       | ++cmd+e++       | `` `code` ``                                       |
| [Code block](#code-block) | ++ctrl+alt+c++   | ++cmd+alt+c++   | ```` ``` code ````<br>or ```` ```{language} code ```` |
| Link                      | ++ctrl+k++       | ++cmd+k++       |                                                    |
|                           |                  |                 |                                                    |
| Undo                      | ++ctrl+z++       | ++cmd+z++       |                                                    |
| Redo                      | ++ctrl+shift+z++ | ++cmd+shift+z++ |                                                    |
|                           |                  |                 |                                                    |
| Cut                       | ++ctrl+x++       | ++cmd+x++       |                                                    |
| Copy                      | ++ctrl+c++       | ++cmd+c++       |                                                    |
| Paste                     | ++ctrl+v++       | ++cmd+v++       |                                                    |
| Paste without formatting  | ++ctrl+shift+v++ | ++cmd+shift+v++ |                                                    |

## Code block

In AskAnna, you can format code blocks by providing the language of the code. When you use Markdown, you can start a
code block with ```` ```{language} ````. For example, to create a code block with Python:

![Editor: code block](media/editor-code-block.gif){ loading=lazy }

You can always change the language formatting used. Click on the language in the right upper corner and select
something else.

We also offer an easy way to copy the code from a block. Click on the copy icon: :material-content-copy:

## Reference

The core of the AskAnna editor is based on [tiptap editor](https://www.tiptap.dev/).
