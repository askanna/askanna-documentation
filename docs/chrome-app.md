# Chrome app

You can install AskAnna as a Chrome app. By installing AskAnna as a Chrome app,
you can easily access AskAnna as if is it one of your software applications.

## Install

In your Chrome browser open AskAnna. In the address bar you will find an option
to install AskAnna. Click on it to install AskAnna:

![Install AskAnna](media/askanna-chrome-app-install.gif)

## Uninstall

When you open the app, you can click on the menu and select
`Uninstall AskAnna...`.

![Uninstall AskAnna](media/askanna-chrome-app-uninstall.gif)
