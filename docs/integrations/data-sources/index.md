# Connect to data sources

The data you need for running your data science jobs might be on several locations. We designed AskAnna to make it
easy to integrate with other tools, platforms, connectors, et cetera. In this section, we describe how you can connect
to several data sources.

You can find information about connecting to data sources for:

1. Databases
    1. [PostgreSQL](postgresql.md)
    2. [MySQL](mysql.md)
    3. [MariaDB](mariadb.md)
    4. [Microsoft SQL Server](mssql.md)
    5. [Snowflake](snowflake.md)
    6. [Google BigQuery](google-bigquery.md)
2. File storage
    1. [Amazon S3](amazon-s3.md)
    2. [Google Cloud Storage](google-cloud-storage.md)

Per data source, we also give a short description of how you can set up your project in AskAnna. We show which
variables you need to set, how you can develop locally and use the variables to run your job in AskAnna without the
need to change your code.

Would you like to see another connector? Or do you need support with connecting to a data source? We love to
[support](../../contact.md) you.
