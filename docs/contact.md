---
hide:
  - navigation
---

# Support

We work hard to improve AskAnna. We cannot do this without your input. You can mention bugs, ideas, improvements...and
also what you like. We hope to hear from you!

And, besides contacting us about AskAnna, feel free to ask and discuss anything with us. We can’t promise that we can
help you with everything ;) What we promise is that you always will get an honest answer from us. And we want to build
AskAnna together with you. Feel free to share your feedback and ideas.

If you don't know how you can do something? Ask us. We have seen a lot, so we might be able to help you. We want you
to succeed in your data science projects and to keep things simple.

## Send us an email

[support@askanna.io](mailto:support@askanna.io)
