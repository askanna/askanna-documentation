# Code

To succeed in a data science project, you probably need code. In AskAnna you can push or upload code to your project.
Besides uploading, you can also view the files including the content of the files.

The code section can appear on several parts in the web interface. The main location is when you open a project. On
the project page you will find a section code. Another location is the run page. On the run page you can find the code
used for the run.

![Code page](media/code/askanna-code-page.png){ loading=lazy }

On the code section you find a bar related to the code with some information and options:

- [Package ID](#package-id) + Breadcrump
- [Download](#download)
- [Replace](#replace)
- [History](#history)

In the table you can find:

| Name     | Description                                                                           |
| -------- | ------------------------------------------------------------------------------------- |
| Name     | The name of a file or directory. [You can click on it to open the file.](#view-files) |
| Size     | The size of the file or files within the directory                                    |
| Modified | The date the file or directory was last modified                                      |

When you are in a directory, the first row will show :material-dots-horizontal:. By clicking on the
:material-dots-horizontal:, you will go back to the parent directory.

## Package ID

Every version of the code is made available in AskAnna as a package. Every new version of the code, is a new package.
The package ID is a unique identifier of the specific version of your code.

In the bar you see by default the first four charactes of the code package unique ID. If you hoover the SUUID, you can
see the full SUUID.

When you navigate through the code, the Package ID can be used as a breadcrump.

## Download

You can download every version of the code. When you click download, you will download a ZIP file containing all files
in the code directory.

## Replace

There are two ways to replace a code version in AskAnna with a new version. When you can click the replace button you
can find th following information as well.

![Replace code](media/code/askanna-code-replace.png){ loading=lazy }

### Push code

When you have installed [the AskAnna CLI](cli.md), you can use a command line interface (a.k.a. terminal) to push code
to AskAnna. The AskAnna CLI will look for a file `askanna.yml`. Every file and directory in the "root directory" where
the `askanna.yml` file is, will be pushed to AskAnna.

As mentioned, the AskAnna CLI will look for a file `askanna.yml`. If this file cannot be found in the current directoy,
the CLI will go one level higher. If a the file cannot be found, the CLI will inform you. If you created a file
`askanna.yml`, you will also need to add the `push-target` as a line in the file.

The `push-target` is the URL of your project. In the replace section you can also copy the line you need to add to your
project. Click replace, look for the line with `push-target` and click the copy buttons on the right side of this line.

When you have done the configuration, you can simply push code to AskAnna via:

```shell
askanna push
```

The AskAnna CLI will ask you a couple of questions. At the end the AskAnna CLI will make a ZIP file of your local
direcoty, and push the code to AskAnna.

#### Adding a description

It is possible to add a push message by adding a description via:

```shell
askanna push -m "a description"
```

When you check the history page you can use this description to know what happened in a specific version.

If your local directory is also initiated as a `git directory`, and you did not add a push message, then the latest git
commit message will appear as the description of the code version.

### Upload a ZIP file

Make a ZIP file from your local code directory. You can drag & drop this file to the dotted area, or you can click
browse. When you click upload, the ZIP file will be uploaded to the AskAnna platform. After it has processed the new
code version is available.

Uploading a new version is background process. You can continue using AskAnna, while uploading continues in the
background. Next to your profile icon (right upper corner) you can find the current status of your upload.

## History

![Code history](media/code/askanna-code-history.png){ loading=lazy }

When you click on `History` you will see a list of all project code versions uploaded or pushed to AskAnna. On the
history page you will find a table with the columns:

| Name         | Description                                             |
| ------------ | ------------------------------------------------------- |
| SUUID        | The unique ID of the Code package                       |
| Date created | When the code was uploaded or pushed to AskAnna         |
| By           | The member who uploaded or pushed to code to AskAnna    |
| Description  | Optionally a description.                               |
| Download     | An option to download a ZIP file containing the version |

By clicking on a row, you can also open the specific code version.

## View files

You can view the content of a code directory. Every available file is listed. Although by default we push `.git`
directories, we will not show them in the interface.

![View file](media/code/askanna-code-view-file.png){ loading=lazy }

You can also view the content of the files in AskAnna. If possible we will format the code. For HTML, JSON and
[Jupyter Notebook](#view-jupyter-notebook) files we show a rendered version that we call pretty view. HTML and JSON
files can also be viewed in their original format by changing the view.

Besides the content of the file, you will also see a file bar. In this bar you see:

- :material-arrow-left: to go back
- Name of the file
- Size of the file
- Button to Download the file
- Button to copy the content of the file
- If possible: switch view between pretty and raw

### View Jupyter Notebook

In AskAnna we support viewing Jupyter Notebooks. When opening a Jupyter Notebook in AskAnna you will see it as a web
page. If a table has more columns then what fits the screen, you can horizontal scroll to see more. Graphs are also
presented as images.

## askanna.yml

The file `askanna.yml` contains the configuration of your project. In [push code](#push-code) we already made a
reference to define the `push-target` in the file `askanna.yml`. Here we will list more options about what you can
configure in the file `askanna.yml`.

The file `askanna.yml` should be located in the **root path** of the code directory.

In this file we use YAML syntax. To learn more about YAML syntax, you can check
[this Ansible page](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html){target="_blank"}.

### Push target

The `push-target` is a line where you define to which projects you want to push the code. An example:

```yaml
push-target: https://beta.askanna.eu/{WORKSPACE_SUUID}/project/{PROJECT_SUUID}/
```

### Job definition

In the askanna.yml file you can define jobs. Below an example. On the documentation page for [Jobs](job/index.md) you
can find more information about [creating jobs](job/create-job.md) via the `askanna.yml`.

```yaml
name of the job:
  job:
    - pip install -r requirements.txt
    - python your_model.py
  output:
    result: models/model_summary.json
    artifact:
      - models/
```

### Validate askanna.yml

When you use the [AskAnna CLI](cli.md) to push code to AskAnna, the CLI will automatically validate the `askanna.yml`
configuration.

The first validation that will run is if the job names are valid. Job names that are not allowed:

- push-target
- environment
- notifications
- cluster
- image
- worker
- variables

[Job schedules](job/schedules.md) will also be checked by the validate function.

When you have a run with a schedule, and you did not specify a time zone in the `askanna.yml`. For these jobs we run
a check if the local system's time zone is not `UTC`. If that's the case, we show an informative message. This message
informs that you could set the time zone to run your jobs in the same time zone as your local system.
[On the page "Create a Job", you can read more about configuring time zones.](job/create-job.md#time-zone)

If you set notifications, we also run a validation if the configuration is valid. Not only the format, we also run
if emails are valid.

## Ignore files

When you use the AskAnna CLI to [push code](#push-code) to your project, you might want to exclude some files or
directories. Inspired by [gitignore](https://git-scm.com/docs/gitignore){target=_blank}, we added support to ignore
files and directories.

If you want to set files and directories that should not be pushed to AskAnna, you can add a file `askannaignore` or
`.askannaignore`. In this file, you list the files and directories that the push command should ignore. Some examples:

- If you add `hello.py` then files with this name will not be pushed to AskAnna.
- The pattern `hello.*` matches any file or folder whose name begins with `hello.`.
- The pattern `foo/` will not push the directory `foo` and paths underneath.
- A prefix `!` will include the file or directory that matches the pattern. If you combine `foo/` with
  `!foo/hello.py`, then all files and subdirectories of the directory `foo/` will not be pushed, except the file
  `foo/hello.py`.

When you set the above examples in a file `askannaignore`, it results in:

```text
hello.py
hello.*
foo/
!foo/hello.py
```

On the [gitignore](https://git-scm.com/docs/gitignore){target=_blank} page, you can find more patterns and examples.

If you manage your code with [git](https://git-scm.com/){target=_blank} and already have a file `.gitignore`, you
don't have to do anything. AskAnna will automatically use the `.gitignore` specification. When AskAnna should not apply
the `.gitignore` file, you can add a file `askannaignore` or `.askannaignore`.

In case you added files named `askannaignore`, `.askannaignore` and `.gitignore`. Then AskAnna will use the first
matching file from the list: `askannaignore`
