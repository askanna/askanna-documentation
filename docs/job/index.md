# Job

In an AskAnna project, you can create jobs that you specify in the [askanna.yml](../code.md#askannayml). A job describe
a task you want to perform using the AskAnna platform.

There are a couple of use cases for jobs in AskAnna:

1. Run a (large) task in AskAnna, so you don’t have to run it locally on your machine
1. If you want to run a series of commands, for example to retrain & update your model, the job is the place to be
1. Using our API and Jobs, you can put a data science model in production and run it via other applications

## Job overview

Jobs relates to a [project](../project.md). On the project page you will find a section listing jobs that are
created for this project.

![Jobs](../media/project/askanna-project-jobs.png){ loading=lazy }

When you open the jobs section, you get a table that lists the available jobs for that project. In this table you find:

| Name                       | Description                                                                            |
| -------------------------- | -------------------------------------------------------------------------------------- |
| Name                       | Name of the job                                                                        |
| Runs                       | How many times the jobs has run                                                        |
| Status                     | The status of the last run. When you click on the status, the latest run for that job opens. |

You can find more details about the table with runs on the [runs](../run/index.md) page.

## Job page

After you have [created a job](create-job.md), you can open the job page related to that job. On this page you will
find the **Overview** with:

- Description of the job
- Definition of the job
- Information how you can [run the job](run-job.md)

Also, you see a section [Runs](../run/index.md) where you can find the runs for that job. On the
[run documentation](../run/index.md) you can find more information regarding this topic.

Below an example of a job page:

![Job page](../media/job/askanna-job-page.png){ loading=lazy }

## Edit job info

Via the web interface, you can edit the information of the job. In addition, via the CLI and Python SDK, you can
update the description and change the job's name as well.

??? warning "Changed the job name? Then update the `askanna.yml` as well."
    Changing the name of a job can be useful in case you want to rename your job. If you are going to rename the job,
    don't forget to rename the job in the [askanna.yml](../code.md#askannayml).

    If you don't do this, you will not be able to run the job anymore because there is no matching job definition in
    the [askanna.yml](../code.md#askannayml) config. And if you don't change it in the
    [askanna.yml](../code.md#askannayml) file, the next time you push a new version of your code to AskAnna we will
    create a new job using the old name.

### Web interface

On the job page you can click on :material-dots-vertical:. In this menu select `EDIT JOB` and you can update the job
info. Here you can also add or update a description of the job. Everybody with access to the job can see this
information.

![Edit job info](../media/job/edit-job-info.gif){ loading=lazy }

!!! tip "Markdown support"
    The text editor support Markdown instructions. These instructions will automatically transferred into formatted
    text.

### CLI

With the [AskAnna CLI](../cli.md) you can change the job info with your command line. In your terminal run:

```shell
askanna job change
```

Optionally you can also directly provide the info you want to change. For example, if you only want to change the
`name`, you don't have to specify the description. If you don't add it, it will not be updated.

```shell
askanna job change --id "{JOB_SUUID}" --name "New job name" --description "New job description"
```

### Python SDK

With the [AskAnna Python SDK](../python-sdk/index.md), you can change the job info using the SUUID of the job:

```python
from askanna import job

job.change(job_suuid="{JOB_SUUID}", name="New job name", description="New job description")
```

## Remove a job

You can remove a job via the web interface, CLI, Python SDK and API. When you remove a job, this is what happens:

- the job will be removed
- all data related to the job, including runs, will be removed
- if other projects, systems or jobs use information or files from the job and related runs, they are not able to use
  it anymore

!!! warning "Recovery period of 30 days"
    When you remove a job, you will not have access to the job anymore. After 30 days, the system will
    entirely delete the job from the backend. Within 30 days, you can [contact us](../contact.md) if you want to
    undo the removal.

    If the job and related files should be removed as soon as possible, you can [contact us](../contact.md).

### Web interface

In the web interface you can remove a job from job page by clicking on :material-dots-vertical: on the right side.
Next select `REMOVE THIS JOB` and confirm that you want to remove the job.

### Command line (CLI)

With the [AskAnna CLI](../cli.md) you can remove a job on the command line. In your terminal run:

```shell
askanna job remove --id "{JOB_SUUID}"
```

### Python SDK

With the [Python SDK](../python-sdk/job.md#askannajobdelete) you can delete a job via:

```python
from askanna import job

job.delete(job_suuid="{JOB_SUUID}")
```

### API

An authenticated user can DELETE a job via:

`DELETE https://beta-api.askanna.eu/v1/job/{JOB_SUUID}/`

If deleting a job was successful, AskAnna will respond with an HTTP-code 204.
