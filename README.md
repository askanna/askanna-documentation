# Introduction

The AskAnna documentation uses [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/). Check out their
site for more information, configuration options and plugins.

[![license](https://img.shields.io/badge/License-CC%20BY--SA%204.0-brightgreen.svg)](https://creativecommons.org/licenses/by-sa/4.0/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

## Run it locally

If you want to view the documentation locally you can install it via pip:

```shell
pip install -r requirements-dev.txt
```

After installing the dependencies you can review the site in a browser. To serve the AskAnna documentation site run the
next command from the project directory:

```shell
mkdocs serve
```

**Note:** when we deploy the documentation site, we use `Material for MkDocs Insiders`. Some of the functions available
in the Insiders version might not be available in the development version.

## Docker for production

For production we use Docker. You can also build a prodution image locally:

First, build the MkDocs site:

```shell
mkdocs build --site-dir public
```

Next, build the Docker image:

```shell
docker build -t askanna-docs/review -f docker/Dockerfile .
```

And run the image so you can (p)review the documentation on [http://localhost:8000](http://localhost:8000):

```shell
docker run --rm -p 80:8000 askanna-docs/review:latest
```
