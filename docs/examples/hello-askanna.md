# Run "Hello AskAnna"

[Open this project in AskAnna][askanna-project]{: .md-button .md-button--primary target=_blank }

[askanna-project]: https://beta.askanna.eu/6swz-ujcr-jQQw-SAdZ/project/71cZ-OfBp-Q2jC-DbRP/

The project "Hello AskAnna" is a simple demo project. The goal is to give you a quick overview of how easy it is to
setup a project in AskAnna. In the "Hello AskAnna" project, we show how you can:

- create a project
- make a "Hello AskAnna" Python script
- define a job that can run the script
- push the code to AskAnna
- run the job in AskAnna

AskAnna has a powerful web interface and for many tasks you can use your browser. But sometimes it's faster to use your
terminal. In this example project we will show both methods. Try them both and figure out which method you like
the most.

If you did not install the CLI yet, on [the AskAnna CLI page](../cli.md) you can read how to install it. Or quickly do
it by running the next commands in your Python environment:

```shell
pip install askanna
```

And ones installed, you can log in by running:

```shell
askanna login
```

## 1. Create the project

We assume you have an AskAnna account and that you are logged in. With the [AskAnna CLI](../cli.md) you can create the
project by running:

```shell
askanna create "Hello AskAnna"
```

This will create a project in AskAnna. Also, it will create a new local directory `hello-askanna` with a file
[askanna.yml](../code.md#askannayml). For the following steps we assume that you work from this new directory.

## 2. Hello-script

In the directory `hello-askanna`, you can add a Python file with a script. For example:

```python
print("Hello, AskAnna!")
```

Save this Python script as `hello.py`, or use the following command to directly create the Python file including the
script:

```shell
echo 'print("Hello, AskAnna!")' > hello.py
```

## 3. Job configuration

Let's [define a job](../job/create-job.md) `hello` that can run the hello-script. You can do this by adding the next
lines to the `askanna.yml` config file:

```yaml
hello:
  job:
    - python hello.py
```

## 4. Push code

In the directory where you build the "Hello AskAnna" project, you can run the command to push the code to AskAnna:

```shell
askanna push
```

Now the AskAnna CLI will package your code and upload this to AskAnna. After the push is finished, you can open your
project in the AskAnna web interface. Here you should see two files. Feel free to click on a file to check the content.

![Code](../media/examples/hello-askanna/code.png)

## 5. Run the Hello-job

If you now click on the `JOBS` section, you will see the job `hello`:

![Job Hello](../media/examples/hello-askanna/job.png)

Click on the job to open the job page. On this page you find a section `Running the job` with information about
how to run the job including an option to run the job directly from the web interface. But for this demo, we go back
to the terminal.

To run the job `hello`, run the following command in your terminal:

```shell
askanna run hello
```

Now you can go back to the job page and in the run section you see a run. Open the run and click on the log tab to
check if the output of the run is indeed `Hello, AskAnna!`.

![Run Log - Hello, AskAnna!](../media/examples/hello-askanna/run-log.png)

!!! success "Congratulations!"
    You now have successfully:

    * created a new project in AskAnna
    * configured a job
    * pushed code to AskAnna
    * run a job
    * opened the job and run in the web interface
