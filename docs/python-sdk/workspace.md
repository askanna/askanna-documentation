# Python SDK - Workspace

## askanna.workspace.get

```python
import askanna

workspace = askanna.workspace.get(workspace_suuid="{WORKSPACE_SUUID}")
```

<h3>Parameters</h3>

| Name            | Type   | Required | Description                                          |
| --------------- | ------ | -------- | ---------------------------------------------------- |
| workspace_suuid | String | Yes      | SUUID of the workspace you want to get the info from |

<h3>Output</h3>

[Dataclass: Workspace](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/workspace.py)

## askanna.workspace.list

```python
import askanna

workspace_list = askanna.workspace.list()
```

<h3>Parameters</h3>

| Name             | Type    | Required | Description                                                             |
| ---------------- | ------- | -------- | ----------------------------------------------------------------------- |
| is_member        | Bool    | No       | Filter on workspaces where the authenticated user is a member.          |
| visibility       | String  | No       | Filter on workspaces with a specific visibility ("PRIVATE" or "PUBLIC") |
| number_of_result | Integer | No       | The number of workspaces in the result. Defaults to 100.                |
| order_by         | String  | No       | Sets the ordering of the workspace list. Defaults to "-created_at".     |
| search           | String  | No       | Search for a specific workspace                                         |

<h3>Output</h3>

List with elements with
[Dataclass: Workspace](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/workspace.py)

## askanna.workspace.create

```python
import askanna

workspace = askanna.workspace.create(name="Workspace name")
```

<h3>Parameters</h3>

| Name        | Type   | Required | Description                                                                       |
| ----------- | ------ | -------- | --------------------------------------------------------------------------------- |
| name        | String | Yes      | Name for the new workspace                                                        |
| description | String | No       | Description for the new workspace. Defaults to an empty description.              |
| visibility  | String | No       | Visibility of the new workspace. Allowed values are "PRIVATE" and "PUBLIC". Defaults to "PRIVATE". |

<h3>Output</h3>

[Dataclass: Workspace](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/workspace.py)

## askanna.workspace.change

```python
import askanna

workspace = askanna.workspace.change(workspace_suuid="{WORKSPACE_SUUID}", name="New workspace name")
```

<h3>Parameters</h3>

| Name            | Type   | Required | Description                                                                   |
| --------------- | ------ | -------- | ----------------------------------------------------------------------------- |
| workspace_suuid | String | Yes      | SUUID of the workspace you want to change                                     |
| name            | String | No       | New name for the workspace. Defaults to None (do not change).                 |
| description     | String | No       | New description for the workspace. Defaults to None (do not change).          |
| visibility      | String | No       | Visibility of the workspace. Allowed values are "PRIVATE", "PUBLIC" or None. Defaults to None (do not change). |

!!! note "To change workspace info, at least we need a new `name`, `description` or `visibility`"

<h3>Output</h3>

[Dataclass: Workspace](https://gitlab.com/askanna/askanna-python/-/blob/master/askanna/core/dataclasses/workspace.py)

## askanna.workspace.delete

```python
import askanna

askanna.workspace.delete(workspace_suuid="{WORKSPACE_SUUID}")
```

<h3>Parameters</h3>

| Name            | Type   | Required | Description                               |
| --------------- | ------ | -------- | ----------------------------------------- |
| workspace_suuid | String | Yes      | SUUID of the workspace you want to delete |

<h3>Output</h3>

Bool: True if the workspace is succesfully deleted
